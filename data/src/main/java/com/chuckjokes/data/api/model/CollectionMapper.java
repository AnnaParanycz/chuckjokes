package com.chuckjokes.data.api.model;

import com.annimon.stream.Stream;
import com.chuckjokes.domain.api.java.stream.ImmutableCollectors;

import java.util.Collections;
import java.util.List;

/**
 * Converts a list of objects of a given type to a list of objects or another type
 * This mapper removes null items
 * <p>
 * SOURCE_MODEL the type of the original collection
 * TARGET_MODEL the type for the new converted collection
 */
public class CollectionMapper {

    private static CollectionMapper collectionMapper;

    public static void setInstance(CollectionMapper collectionMapper) {
        CollectionMapper.collectionMapper = collectionMapper;
    }

    private static CollectionMapper getCollectionMapper() {
        if (collectionMapper == null) {
            collectionMapper = new CollectionMapper();
        }
        return collectionMapper;
    }

    <SOURCE_MODEL, TARGET_MODEL> List<TARGET_MODEL> mapArray(
            Mapper<SOURCE_MODEL, TARGET_MODEL> listItemMapper,
            SOURCE_MODEL[] from) {

        if (from == null) {
            return Collections.emptyList();
        }
        return Stream.of(from)
                .filter(toBeMapped -> toBeMapped != null)
                .map(listItemMapper::map)
                .filter(value -> value != null)
                .collect(ImmutableCollectors.toList());
    }

    public <SOURCE_MODEL, TARGET_MODEL> List<TARGET_MODEL> mapList(
            Mapper<SOURCE_MODEL, TARGET_MODEL> listItemMapper,
            Iterable<SOURCE_MODEL> from) {

        if (from == null) {
            return Collections.emptyList();
        }
        return Stream.of(from)
                .filter(toBeMapped -> toBeMapped != null)
                .map(listItemMapper::map)
                .filter(value -> value != null)
                .collect(ImmutableCollectors.toList());
    }

    public static <SOURCE_MODEL, TARGET_MODEL> List<TARGET_MODEL> map(
            Mapper<SOURCE_MODEL, TARGET_MODEL> listItemMapper,
            List<SOURCE_MODEL> from) {

        return getCollectionMapper().mapList(listItemMapper, from);
    }

    public static <SOURCE_MODEL, TARGET_MODEL> List<TARGET_MODEL> map(
            Mapper<SOURCE_MODEL, TARGET_MODEL> listItemMapper,
            SOURCE_MODEL[] from) {

        return getCollectionMapper().mapArray(listItemMapper, from);
    }
}
