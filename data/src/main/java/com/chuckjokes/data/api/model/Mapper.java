package com.chuckjokes.data.api.model;

public interface Mapper<FROM, TO> {

    TO map(FROM from);
}
