package com.chuckjokes.data.joke.local.database.service;

import com.chuckjokes.data.joke.local.database.model.JokeBean;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface JokeDatabaseService {

    Completable putJokes(List<JokeBean> jokes);

    Completable putJoke(JokeBean joke);

    Single<List<JokeBean>> getJokes();

    Single<JokeBean> getJoke(String id);
}
