package com.chuckjokes.data.joke.local.database;

import com.annimon.stream.Stream;
import com.chuckjokes.data.api.model.CollectionMapper;
import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.local.LocalJokeRepository;
import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.data.joke.local.database.service.JokeDatabaseService;
import com.chuckjokes.domain.api.java.stream.ImmutableCollectors;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;
import com.chuckjokes.domain.usecase.name.entity.Name;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class LocalDatabaseJokeRepository implements LocalJokeRepository {

    private static final String PERSONALISED = "pers_";

    private final JokeDatabaseService jokeDatabaseService;
    private final Mapper<Joke, JokeBean> jokeToBeanMapper;
    private final Mapper<JokeBean, Joke> jokeMapper;

    public LocalDatabaseJokeRepository(JokeDatabaseService jokeDatabaseService,
                                       Mapper<Joke, JokeBean> jokeToBeanMapper,
                                       Mapper<JokeBean, Joke> jokeMapper) {

        this.jokeDatabaseService = jokeDatabaseService;
        this.jokeToBeanMapper = jokeToBeanMapper;
        this.jokeMapper = jokeMapper;
    }

    @Override
    public Completable putJoke(Joke joke) {
        return jokeDatabaseService.putJoke(jokeToBeanMapper.map(joke));
    }

    @Override
    public Completable putJokes(List<Joke> jokes) {
        return jokeDatabaseService.putJokes(CollectionMapper.map(jokeToBeanMapper, jokes));
    }

    @Override
    public Single<List<Joke>> getListOfJokes(ListOfJokesRequest request) {
        return jokeDatabaseService.getJokes().map(jokes -> {
            Collections.shuffle(new ArrayList<>(jokes));
            return Stream.of(jokes)
                    .limit(request.getCount())
                    .map(jokeMapper::map)
                    .collect(ImmutableCollectors.toList());
        });
    }

    @Override
    public Single<Joke> getJoke(String id) {
        return jokeDatabaseService.getJoke(id).map(jokeMapper::map);
    }

    @Override
    public Single<Joke> getRandomJoke() {
        return jokeDatabaseService.getJokes()
                .map(jokeBeans -> jokeBeans.get(new Random().nextInt(jokeBeans.size())))
                .map(jokeMapper::map);
    }

    @Override
    public Single<Joke> getRandomPersonalisedJoke(RandomPersonalisedJokeRequest request) {
        return getRandomJoke().map(joke -> {
            final Name name = request.getName();
            return new Joke(joke.getDisplayText()
                    .replace("Chuck", name.getFirstName())
                    .replace("Norris", name.getLastName()),
                    PERSONALISED + joke.getId(),
                    joke.getCategories());
        });
    }
}
