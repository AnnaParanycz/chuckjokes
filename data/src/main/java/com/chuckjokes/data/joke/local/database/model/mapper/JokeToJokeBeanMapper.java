package com.chuckjokes.data.joke.local.database.model.mapper;

import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

public class JokeToJokeBeanMapper implements Mapper<Joke, JokeBean> {

    @Override
    public JokeBean map(Joke joke) {
        if (joke == null) {
            return null;
        }
        final JokeBean jokeBean = new JokeBean();
        jokeBean.setId(joke.getId());
        jokeBean.setDisplayText(joke.getDisplayText());
        jokeBean.setCategories(joke.getCategories());
        return jokeBean;
    }
}
