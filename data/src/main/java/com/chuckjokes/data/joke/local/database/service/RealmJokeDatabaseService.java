package com.chuckjokes.data.joke.local.database.service;

import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.data.joke.local.database.model.JokeTable;
import com.chuckjokes.domain.usecase.joke.entity.NoJokesAvailableException;

import java.util.List;

import javax.inject.Provider;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmJokeDatabaseService implements JokeDatabaseService {

    private final Provider<Realm> realmProvider;
    private final Scheduler scheduler;
    private Realm realm;

    public RealmJokeDatabaseService(Provider<Realm> realmProvider,
                                    Scheduler scheduler) {
        this.realmProvider = realmProvider;
        this.scheduler = scheduler;
    }

    @Override
    public Completable putJokes(List<JokeBean> jokes) {
        return Completable.create(e -> {
            try {
                getRealm().beginTransaction();
                getRealm().copyToRealmOrUpdate(jokes);
                getRealm().commitTransaction();
                e.onComplete();
            } catch (Exception exception) {
                e.onError(exception);
            }
        }).subscribeOn(scheduler);
    }

    @Override
    public Completable putJoke(JokeBean joke) {
        return Completable.create(e -> {
            try {
                getRealm().beginTransaction();
                getRealm().copyToRealmOrUpdate(joke);
                getRealm().commitTransaction();
                e.onComplete();
            } catch (Exception exception) {
                e.onError(exception);
            }
        }).subscribeOn(scheduler);
    }

    @Override
    public Single<List<JokeBean>> getJokes() {
        return Single.<List<JokeBean>>create(e -> {
            try {
                final RealmResults<JokeBean> jokes = getRealm().where(JokeBean.class).findAll();
                if (jokes != null && !jokes.isEmpty()) {
                    e.onSuccess(jokes);
                } else {
                    e.onError(new NoJokesAvailableException());
                }
            } catch (RuntimeException exception) {
                e.onError(exception);
            }
        }).subscribeOn(scheduler);
    }

    @Override
    public Single<JokeBean> getJoke(String id) {
        return Single.<JokeBean>create(e -> {
            try {
                final JokeBean joke = getRealm().where(JokeBean.class).equalTo(JokeTable.id, id).findFirst();
                if (joke != null) {
                    e.onSuccess(joke);
                } else {
                    e.onError(new NoJokesAvailableException());
                }
            } catch (RuntimeException exception) {
                e.onError(exception);
            }
        }).subscribeOn(scheduler);
    }

    private Realm getRealm() {
        if (realm == null) {
            realm = realmProvider.get();
        }
        return realm;
    }
}
