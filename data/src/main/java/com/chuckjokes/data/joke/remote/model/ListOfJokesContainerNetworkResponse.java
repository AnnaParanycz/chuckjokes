package com.chuckjokes.data.joke.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("SameParameterValue")
public class ListOfJokesContainerNetworkResponse {

    @SerializedName("value")
    private final List<JokeNetworkResponse> jokes;
    private final String type;

    public ListOfJokesContainerNetworkResponse(List<JokeNetworkResponse> jokes,
                                               String type) {
        this.jokes = jokes;
        this.type = type;
    }

    public List<JokeNetworkResponse> getJokes() {
        return jokes;
    }

    @SuppressWarnings("unused")
    public String getType() {
        return type;
    }
}
