package com.chuckjokes.data.joke.remote.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("SameParameterValue")
public class JokeContainerNetworkResponse {

    @SerializedName("value")
    private final JokeNetworkResponse joke;
    private final String type;

    public JokeContainerNetworkResponse(JokeNetworkResponse joke,
                                        String type) {
        this.joke = joke;
        this.type = type;
    }

    public JokeNetworkResponse getJoke() {
        return joke;
    }

    @SuppressWarnings("unused")
    public String getType() {
        return type;
    }
}
