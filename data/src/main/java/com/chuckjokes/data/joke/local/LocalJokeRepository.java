package com.chuckjokes.data.joke.local;

import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

import io.reactivex.Completable;

public interface LocalJokeRepository extends JokeRepository {

    Completable putJoke(Joke joke);

    Completable putJokes(List<Joke> jokes);
}
