package com.chuckjokes.data.joke.remote.model.mapper;

import com.annimon.stream.Stream;
import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.remote.model.JokeNetworkResponse;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

import static com.chuckjokes.domain.api.util.StringUtil.EMPTY;

public class JokeMapper implements Mapper<JokeNetworkResponse, Joke> {

    private static final String QUOTE_CODE = "&quot;";
    private static final String DISPLAYABLE_QUOTE = "\"";

    @Override
    public Joke map(JokeNetworkResponse response) {
        if (response == null) {
            return new Joke(EMPTY, EMPTY, EMPTY);
        }
        return new Joke(getCleanedText(response), response.getId(), getCategories(response.getCategories()));
    }

    private String getCategories(List<String> categories) {
        StringBuilder categoriesDisplayText = new StringBuilder();
        Stream.of(categories).forEach(s -> categoriesDisplayText.append(s).append(", "));
        return categoriesDisplayText.toString();
    }

    private String getCleanedText(JokeNetworkResponse response) {
        return response.getText().replace(QUOTE_CODE, DISPLAYABLE_QUOTE);
    }
}
