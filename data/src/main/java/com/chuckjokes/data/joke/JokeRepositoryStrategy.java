package com.chuckjokes.data.joke;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.chuckjokes.data.joke.local.LocalJokeRepository;
import com.chuckjokes.data.joke.remote.RemoteJokeRepository;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;

import java.util.List;

import io.reactivex.Single;

public class JokeRepositoryStrategy implements JokeRepository {

    private final RemoteJokeRepository remoteJokeRepository;
    private final LocalJokeRepository localJokeRepository;
    private final ConnectivityManager connectivityManager;

    public JokeRepositoryStrategy(RemoteJokeRepository remoteJokeRepository,
                                  LocalJokeRepository localJokeRepository,
                                  ConnectivityManager connectivityManager) {
        this.remoteJokeRepository = remoteJokeRepository;
        this.localJokeRepository = localJokeRepository;
        this.connectivityManager = connectivityManager;
    }

    @Override
    public Single<List<Joke>> getListOfJokes(ListOfJokesRequest request) {
        return ifNetworkConnected().flatMap(connected -> remoteJokeRepository.getListOfJokes(request))
                .doAfterSuccess(jokes -> localJokeRepository.putJokes(jokes).subscribe())
                .onErrorResumeNext(localJokeRepository.getListOfJokes(request));
    }

    @Override
    public Single<Joke> getRandomPersonalisedJoke(RandomPersonalisedJokeRequest request) {
        return ifNetworkConnected().flatMap(connected -> remoteJokeRepository.getRandomPersonalisedJoke(request))
                .onErrorResumeNext(localJokeRepository.getRandomPersonalisedJoke(request));
    }

    @Override
    public Single<Joke> getJoke(String id) {
        return ifNetworkConnected().flatMap(connected -> remoteJokeRepository.getJoke(id))
                .doAfterSuccess(joke -> localJokeRepository.putJoke(joke).subscribe())
                .onErrorResumeNext(localJokeRepository.getJoke(id));
    }

    @Override
    public Single<Joke> getRandomJoke() {
        return ifNetworkConnected().flatMap(connected -> remoteJokeRepository.getRandomJoke())
                .doAfterSuccess(joke -> localJokeRepository.putJoke(joke).subscribe())
                .onErrorResumeNext(localJokeRepository.getRandomJoke());
    }

    private Single<Boolean> ifNetworkConnected() {
        return Single.create(e -> {
            final NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                e.onSuccess(true);
            } else {
                e.onError(new RuntimeException("No internet"));
            }
        });
    }
}
