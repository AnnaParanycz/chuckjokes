package com.chuckjokes.data.joke.remote;

import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.remote.model.JokeContainerNetworkResponse;
import com.chuckjokes.data.joke.remote.model.JokeNetworkResponse;
import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestService;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;
import com.chuckjokes.domain.usecase.name.entity.Name;

import java.util.List;

import io.reactivex.Single;

import static com.chuckjokes.data.api.model.CollectionMapper.map;

public class RemoteJokeRepository implements JokeRepository {

    private final RetrofitRemoteRestService retrofitRemoteRestService;
    private final Mapper<JokeNetworkResponse, Joke> jokeMapper;

    public RemoteJokeRepository(RetrofitRemoteRestService retrofitRemoteRestService,
                                Mapper<JokeNetworkResponse, Joke> jokeMapper) {

        this.retrofitRemoteRestService = retrofitRemoteRestService;
        this.jokeMapper = jokeMapper;
    }

    @Override
    public Single<List<Joke>> getListOfJokes(ListOfJokesRequest request) {
        return retrofitRemoteRestService.getListOfJokes(request.getCount())
                .map(responses -> map(jokeMapper, responses.getJokes()));
    }

    @Override
    public Single<Joke> getRandomJoke() {
        return retrofitRemoteRestService.getRandomJoke()
                .map(JokeContainerNetworkResponse::getJoke)
                .map(jokeMapper::map);
    }

    @Override
    public Single<Joke> getRandomPersonalisedJoke(RandomPersonalisedJokeRequest request) {
        final Name name = request.getName();
        return retrofitRemoteRestService.getRandomPersonalisedJoke(name.getFirstName(), name.getLastName())
                .map(JokeContainerNetworkResponse::getJoke)
                .map(jokeMapper::map);
    }

    @Override
    public Single<Joke> getJoke(String id) {
        return retrofitRemoteRestService.getJoke(id)
                .map(JokeContainerNetworkResponse::getJoke)
                .map(jokeMapper::map);
    }
}
