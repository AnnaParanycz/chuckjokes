package com.chuckjokes.data.joke.remote.service.retrofit;

import com.chuckjokes.data.joke.remote.model.JokeContainerNetworkResponse;
import com.chuckjokes.data.joke.remote.model.ListOfJokesContainerNetworkResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitRemoteRestService {

    @GET("/jokes/{id}")
    Single<JokeContainerNetworkResponse> getJoke(@Path("id") String id);

    @GET("jokes/random")
    Single<JokeContainerNetworkResponse> getRandomJoke();

    @GET("jokes/random")
    Single<JokeContainerNetworkResponse> getRandomPersonalisedJoke(
            @Query("firstName") String firstName,
            @Query("lastName") String lastName);

    @GET("jokes/random/{count}")
    Single<ListOfJokesContainerNetworkResponse> getListOfJokes(@Path("count") int count);
}
