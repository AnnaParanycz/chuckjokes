package com.chuckjokes.data.joke.local.database.model;

public final class JokeTable {

    private JokeTable() {
    }

    public static final String id = "id";

    public static final String displayText = "displayText";

    public static final String categories = "categories";
}
