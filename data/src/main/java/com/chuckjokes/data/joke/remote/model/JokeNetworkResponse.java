package com.chuckjokes.data.joke.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("SameParameterValue")
public class JokeNetworkResponse {

    private final List<String> categories;

    @SerializedName("joke")
    private final String text;
    private final String id;

    public JokeNetworkResponse(List<String> categories,
                               String text,
                               String id) {
        this.categories = categories;
        this.text = text;
        this.id = id;
    }

    @SuppressWarnings("unused")
    public List<String> getCategories() {
        return categories;
    }

    public String getText() {
        return text;
    }

    public String getId() {
        return id;
    }
}
