package com.chuckjokes.data.joke.local.database.model.mapper;

import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

public class JokeBeanMapper implements Mapper<JokeBean, Joke> {
    @Override
    public Joke map(JokeBean jokeBean) {
        if (jokeBean == null) {
            return null;
        }
        return new Joke(jokeBean.getDisplayText(), jokeBean.getId(), jokeBean.getCategories());
    }
}
