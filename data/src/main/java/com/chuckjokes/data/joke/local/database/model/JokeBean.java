package com.chuckjokes.data.joke.local.database.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class JokeBean extends RealmObject {

    @PrimaryKey
    @Required
    private String id;

    @Required
    private String displayText;

    private String categories;

    public String getDisplayText() {
        return displayText;
    }

    public String getCategories() {
        return categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }
}
