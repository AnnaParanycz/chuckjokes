package com.chuckjokes.data.name.namevalidation.local;

import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.chuckjokes.data.R;
import com.chuckjokes.domain.api.util.StringUtil;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;
import com.chuckjokes.domain.usecase.name.entity.InvalidNameException;
import com.chuckjokes.domain.usecase.name.entity.Name;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.model.NameValidationRequest;

import java.util.Locale;

import io.reactivex.Single;

public class InMemoryNameValidationRepository implements NameValidationRepository {

    private static final int FIRST_AND_LAST_NAME_SPLIT_LIMIT = 2;
    private final Resources resources;

    public InMemoryNameValidationRepository(Resources resources) {
        this.resources = resources;
    }

    @Override
    public Single<Name> getValidatedName(NameValidationRequest nameValidationRequest) {
        if (isRequestInvalid(nameValidationRequest)) {
            return getError(R.string.missing_personalised_joke_text);
        }

        final String[] names = nameValidationRequest.getUserInput().trim().split(StringUtil.SPACE, FIRST_AND_LAST_NAME_SPLIT_LIMIT);
        if (isFirstOrLastNameMissing(names)) {
            return getError(R.string.missing_first_or_last_name_joke_text);
        }

        final String firstName = names[0].trim();
        final String lastName = names[1].trim();

        if (areNamesEmpty(firstName, lastName)) {
            return getError(R.string.missing_first_or_last_name_joke_text);
        }

        if (areNamesNotBegginingWithLetter(firstName, lastName)) {
            return getError(R.string.names_must_start_with_a_letter);
        }

        return Single.just(new Name(capitalise(firstName), capitalise(lastName)));
    }

    private String capitalise(String string) {
        return string.substring(0, 1).toUpperCase(Locale.getDefault()) + string.substring(1);
    }

    private boolean areNamesNotBegginingWithLetter(String firstName, String lastName) {
        return !Character.isLetter(firstName.charAt(0)) || !Character.isLetter(lastName.charAt(0));
    }

    private boolean areNamesEmpty(String firstName, String lastName) {
        return StringUtil.isBlank(firstName) || StringUtil.isBlank(lastName);
    }

    private boolean isFirstOrLastNameMissing(String[] names) {
        return names.length < FIRST_AND_LAST_NAME_SPLIT_LIMIT;
    }

    @NonNull
    private Single<Name> getError(int stringId) {
        return Single.error(new InvalidNameException(resources.getString(stringId)));
    }

    private boolean isRequestInvalid(NameValidationRequest nameValidationRequest) {
        return nameValidationRequest == null || StringUtil.isBlank(nameValidationRequest.getUserInput());
    }
}
