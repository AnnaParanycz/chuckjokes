package com.chuckjokes.data.joke.remote.model.mapper;

import com.chuckjokes.data.joke.remote.model.JokeNetworkResponse;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class JokeMapperTest {

    private final List<String> categories = Arrays.asList("Cat1", "Cat2", "Cat3");
    private final String categoriesDisplay = "Cat1, Cat2, Cat3, ";

    private final JokeNetworkResponse response = new JokeNetworkResponse(categories, "Joke text", "Joke id");

    private JokeMapper jokeMapper;

    @Before
    public void setup() {
        jokeMapper = new JokeMapper();
    }

    @Test
    public void should_map_null_to_empty_joke() {
        // when
        final Joke joke = jokeMapper.map(null);

        // then
        assertThat(joke).isNotNull();
        assertThat(joke.getDisplayText()).isEmpty();
        assertThat(joke.getId()).isEmpty();
    }

    @Test
    public void should_map_response_to_joke() {
        // when
        final Joke joke = jokeMapper.map(response);

        // then
        assertThat(joke).isNotNull();
        assertThat(joke.getDisplayText()).isEqualTo(response.getText());
        assertThat(joke.getId()).isEqualTo(response.getId());
        assertThat(joke.getCategories()).isEqualTo(categoriesDisplay);
    }
}
