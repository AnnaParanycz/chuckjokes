package com.chuckjokes.data.joke.local.database;

import android.support.annotation.NonNull;
import com.chuckjokes.data.api.model.CollectionMapper;
import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.data.joke.local.database.service.JokeDatabaseService;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocalDatabaseJokeRepositoryTest {

    private LocalDatabaseJokeRepository repository;

    @Mock
    private JokeDatabaseService jokeDatabaseService;
    @Mock
    private Mapper<Joke, JokeBean> jokeToBeanMapper;
    @Mock
    private Mapper<JokeBean, Joke> jokeMapper;
    @Mock
    private CollectionMapper collectionMapper;
    @Mock
    private List<Joke> jokes;
    @Mock
    private Joke joke;

    @Before
    public void setup() {
        CollectionMapper.setInstance(collectionMapper);
        repository = new LocalDatabaseJokeRepository(jokeDatabaseService, jokeToBeanMapper, jokeMapper);
    }

    @Test
    public void should_put_one_joke_to_database() {
        // given
        final JokeBean jokeBean = givenJokeToBeanMapperResult();

        // when
        repository.putJoke(joke);

        // then
        verify(jokeToBeanMapper).map(joke);
        verify(jokeDatabaseService, only()).putJoke(jokeBean);
    }

    @Test
    public void should_put_jokes_to_database() {
        // given
        final List<JokeBean> jokeBeans = givenJokeToBeanListMapperResult();

        // when
        repository.putJokes(jokes);

        // then
        verify(collectionMapper).mapList(jokeToBeanMapper, jokes);
        verify(jokeDatabaseService, only()).putJokes(jokeBeans);
    }

    @Test
    public void should_get_random_jokes_list_of_given_size() {
        // given
        final List<JokeBean> jokeBeans = givenDatabaseWillReturnLargeListOfJokes();
        final ListOfJokesRequest request = givenListOfJokesRequest();
        givenMapperWillMapCorrectly();
        assertThat(request.getCount()).isLessThan(jokeBeans.size());

        // when
        TestObserver<List<Joke>> observer = Observable.merge(
                repository.getListOfJokes(request).toObservable(),
                repository.getListOfJokes(request).toObservable()).test();

        // then
        final Predicate<List<Joke>> valuePredicate = new Predicate<List<Joke>>() {
            List<Joke> firstJokes;

            @Override
            public boolean test(List<Joke> jokes) throws Exception {
                assertThat(jokes).isNotNull().hasSize(request.getCount());

                if (firstJokes == null) {
                    firstJokes = jokes;
                } else {
                    assertThat(jokes).isNotEqualTo(firstJokes);
                }
                return true;
            }
        };

        observer.assertValueCount(2);
        observer.assertValueAt(0, valuePredicate);
        observer.assertValueAt(1, valuePredicate);
        observer.assertNoErrors();
    }

    @Test
    public void should_get_joke_per_id() {
        // given
        final String id = "existing id";
        givenDatabaseHasJokeWithId(id);
        givenMapperWillMapCorrectly();

        // when
        TestObserver<Joke> observer = repository.getJoke(id).test();

        // then
        observer.assertValue(receivedJoke -> id.equals(receivedJoke.getId()));
        observer.assertNoErrors();
    }

    @Test
    public void should_get_return_error_when_no_joke_with_given_id_found() {
        // given
        final String id = "this id doesn't exist";
        givenDatabaseDoesNotHaveJokeWithId(id);
        givenMapperWillMapCorrectly();

        // when
        TestObserver<Joke> observer = repository.getJoke(id).test();

        // then
        observer.assertNoValues();
        observer.assertErrorMessage("No joke found");
    }

    @Test
    public void should_get_random_joke() {
        // given
        givenDatabaseWillReturnLargeListOfJokes();
        givenMapperWillMapCorrectly();

        // when
        TestObserver<Joke> observer = repository.getRandomJoke().test();

        // then
        observer.assertValue(joke1 -> joke1 != null);
        observer.assertNoErrors();
    }

    // region helper
    private ListOfJokesRequest givenListOfJokesRequest() {
        return new ListOfJokesRequest(2);
    }

    private JokeBean givenJokeToBeanMapperResult() {
        final JokeBean jokeBean = new JokeBean();
        when(jokeToBeanMapper.map(any())).thenReturn(jokeBean);
        return jokeBean;
    }

    private List<JokeBean> givenJokeToBeanListMapperResult() {
        final List<JokeBean> jokeBeans = mock(List.class);
        when(collectionMapper.<Joke, JokeBean>mapList(any(), any())).thenReturn(jokeBeans);
        return jokeBeans;
    }

    private void givenDatabaseHasJokeWithId(String id) {
        final JokeBean item = new JokeBean();
        item.setId(id);
        when(jokeDatabaseService.getJoke(id)).thenReturn(Single.just(item));
    }

    private void givenDatabaseDoesNotHaveJokeWithId(String id) {
        when(jokeDatabaseService.getJoke(id)).thenReturn(Single.error(new RuntimeException("No joke found")));
    }

    private List<JokeBean> givenDatabaseWillReturnLargeListOfJokes() {
        final List<JokeBean> jokeBeans = Arrays.asList(
                createJokeBean("d0", "i0", "c0"),
                createJokeBean("d1", "i1", "c1"),
                createJokeBean("d2", "i2", "c2"),
                createJokeBean("d3", "i3", "c3"),
                createJokeBean("d4", "i4", "c4"),
                createJokeBean("d5", "i5", "c5"),
                createJokeBean("d6", "i6", "c6"),
                createJokeBean("d7", "i7", "c7"),
                createJokeBean("d8", "i8", "c8"),
                createJokeBean("d9", "i9", "c9"),
                createJokeBean("d10", "i10", "c10"),
                createJokeBean("d11", "i11", "c11"),
                createJokeBean("d12", "i12", "c12"),
                createJokeBean("d13", "i13", "c13"),
                createJokeBean("d14", "i14", "c14"),
                createJokeBean("d15", "i15", "c15"),
                createJokeBean("d16", "i16", "c16"),
                createJokeBean("d17", "i17", "c17"),
                createJokeBean("d18", "i18", "c18"),
                createJokeBean("d19", "i19", "c19"),
                createJokeBean("d20", "i20", "c20")
        );
        when(jokeDatabaseService.getJokes()).thenReturn(Single.just(jokeBeans));
        return jokeBeans;
    }

    private void givenMapperWillMapCorrectly() {
        when(jokeMapper.map(any())).thenAnswer(new Answer<Joke>() {
            @Override
            public Joke answer(InvocationOnMock invocation) throws Throwable {
                final JokeBean jokeBean = (JokeBean) invocation.getArguments()[0];
                return new Joke(jokeBean.getDisplayText(), jokeBean.getId(), jokeBean.getCategories());
            }
        });
    }

    @NonNull
    private JokeBean createJokeBean(String text, String id, String comment) {
        final JokeBean jokeBean = new JokeBean();
        jokeBean.setId(id);
        jokeBean.setCategories(text);
        jokeBean.setDisplayText(comment);
        return jokeBean;
    }
    // endregion
}
