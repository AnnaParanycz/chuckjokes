package com.chuckjokes.data.joke.remote.service.retrofit;

import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestService;
import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestServiceFactory;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class RetrofitRemoteRestServiceFactoryTest {

    private RetrofitRemoteRestServiceFactory retrofitRemoteRestServiceFactory;

    @Before
    public void setup() {
        givenRetrofitServiceFactory();
    }

    private void givenRetrofitServiceFactory() {
        retrofitRemoteRestServiceFactory = new RetrofitRemoteRestServiceFactory();
    }

    @Test
    public void create_a_correct_RetrofitRemoteRestServiceFactory() {
        // when
        final RetrofitRemoteRestService retrofitRemoteRestService = retrofitRemoteRestServiceFactory.create();

        // then
        assertThat(retrofitRemoteRestService, CoreMatchers.notNullValue());
    }
}
