package com.chuckjokes.data.joke.local.database.model.mapper;

import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JokeBeanMapperTest {

    private JokeBeanMapper jokeBeanMapper;

    private JokeBean jokeBean;

    @Before
    public void setup() {
        jokeBeanMapper = new JokeBeanMapper();

        jokeBean = new JokeBean();
        jokeBean.setDisplayText("joke_bean_display_text");
        jokeBean.setCategories("joke_bean_categories");
        jokeBean.setId("joke_bean_id");
    }

    @Test
    public void should_return_null_when_joke_bean_is_null() {
        // when
        final Joke joke = jokeBeanMapper.map(null);

        // then
        assertThat(joke).isNull();
    }

    @Test
    public void should_map_to_joke_correctly() {
        // when
        final Joke joke = jokeBeanMapper.map(jokeBean);

        // then
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(joke).isNotNull();
        soft.assertThat(joke.getDisplayText()).isEqualTo(jokeBean.getDisplayText());
        soft.assertThat(joke.getCategories()).isEqualTo(jokeBean.getCategories());
        soft.assertThat(joke.getId()).isEqualTo(jokeBean.getId());
        soft.assertAll();
    }
}