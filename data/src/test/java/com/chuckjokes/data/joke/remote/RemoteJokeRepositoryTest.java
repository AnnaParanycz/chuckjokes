package com.chuckjokes.data.joke.remote;

import android.support.annotation.NonNull;

import com.chuckjokes.data.api.model.CollectionMapper;
import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestService;
import com.chuckjokes.data.joke.remote.model.JokeContainerNetworkResponse;
import com.chuckjokes.data.joke.remote.model.JokeNetworkResponse;
import com.chuckjokes.data.joke.remote.model.ListOfJokesContainerNetworkResponse;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;
import com.chuckjokes.domain.usecase.name.entity.Name;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemoteJokeRepositoryTest {

    private static final String JOKE_ID = "joke_id";
    private static final int COUNT = 5;

    private final ListOfJokesRequest listOfJokesRequest = new ListOfJokesRequest(COUNT);
    private final TestObserver<List<Joke>> listOfJokesObserver = new TestObserver<>();
    private final TestObserver<Joke> jokeObserver = new TestObserver<>();
    private final Name name = new Name("first name", "last name");

    private RemoteJokeRepository remoteJokeRepository;

    @Mock
    private Mapper<JokeNetworkResponse, Joke> jokeMapper;
    @Mock
    private RandomPersonalisedJokeRequest request;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RetrofitRemoteRestService restService;
    @Mock
    private CollectionMapper collectionMapper;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CollectionMapper.setInstance(collectionMapper);

        remoteJokeRepository = new RemoteJokeRepository(restService, jokeMapper);
    }

    @Test
    public void should_request_jokes_from_rest_service() {
        // given
        givenRestServiceWillReturnSuccessfulJokes();
        givenMapperWillMapResponseToEntityJoke();

        // when
        remoteJokeRepository.getListOfJokes(listOfJokesRequest);

        // then
        verify(restService, only()).getListOfJokes(listOfJokesRequest.getCount());
    }

    @Test
    public void should_returned_mapped_jokes_response() {
        // given
        final ListOfJokesContainerNetworkResponse response = givenRestServiceWillReturnSuccessfulJokes();
        final List<Joke> jokes = givenMapperWillMapResponseToEntityJoke();

        // when
        remoteJokeRepository.getListOfJokes(listOfJokesRequest).subscribe(listOfJokesObserver);

        // then
        verify(collectionMapper).mapList(jokeMapper, response.getJokes());
        listOfJokesObserver.assertNoErrors();
        listOfJokesObserver.assertValue(jokes);
    }

    @Test
    public void should_request_random_joke_from_rest_service() {
        // when
        remoteJokeRepository.getRandomJoke();

        // then
        verify(restService, only()).getRandomJoke();
    }

    @Test
    public void should_return_mapped_random_joke_response() {
        // given
        final JokeContainerNetworkResponse response = givenRestServiceWillReturnSuccessfulRandomJoke();
        final Joke joke = givenMapperWillReturnJoke();

        // when
        remoteJokeRepository.getRandomJoke().subscribe(jokeObserver);

        // then
        verify(jokeMapper).map(response.getJoke());
        jokeObserver.assertValue(joke);
        jokeObserver.assertNoErrors();
    }

    public void should_request_joke_from_rest_service() {
        // when
        remoteJokeRepository.getJoke(JOKE_ID);

        // then
        verify(restService, only()).getJoke(JOKE_ID);
    }

    @Test
    public void should_return_mapped_joke_response() {
        // given
        final JokeContainerNetworkResponse response = givenRestServiceWillReturnSuccessfulJoke();
        final Joke joke = givenMapperWillReturnJoke();

        // when
        remoteJokeRepository.getJoke(JOKE_ID).subscribe(jokeObserver);

        // then
        verify(jokeMapper).map(response.getJoke());
        jokeObserver.assertValue(joke);
        jokeObserver.assertNoErrors();
    }


    @Test
    public void should_request_random_personalised_joke_from_rest_service() {
        // given
        givenPersonalisedJokeRequestIsValid();

        // when
        remoteJokeRepository.getRandomPersonalisedJoke(request);

        // then
        verify(request).getName();
        verify(restService, only()).getRandomPersonalisedJoke(name.getFirstName(), name.getLastName());
    }

    @Test
    public void should_return_mapped_random_personalised_joke_response() {
        // given
        givenPersonalisedJokeRequestIsValid();
        final JokeContainerNetworkResponse response = givenRestServiceWillReturnSuccessfulRandomPersonalisedJoke();
        final Joke joke = givenMapperWillReturnJoke();

        // when
        remoteJokeRepository.getRandomPersonalisedJoke(request).subscribe(jokeObserver);

        // then
        verify(jokeMapper).map(response.getJoke());
        jokeObserver.assertValue(joke);
        jokeObserver.assertNoErrors();
    }

    private void givenPersonalisedJokeRequestIsValid() {
        when(request.getName()).thenReturn(name);
    }

    private JokeContainerNetworkResponse givenRestServiceWillReturnSuccessfulJoke() {
        final JokeContainerNetworkResponse response = new JokeContainerNetworkResponse(null, "type");
        when(restService.getJoke(any())).thenReturn(Single.just(response));
        return response;
    }

    private JokeContainerNetworkResponse givenRestServiceWillReturnSuccessfulRandomJoke() {
        final JokeContainerNetworkResponse response = new JokeContainerNetworkResponse(null, "type");
        when(restService.getRandomJoke()).thenReturn(Single.just(response));
        return response;
    }

    private JokeContainerNetworkResponse givenRestServiceWillReturnSuccessfulRandomPersonalisedJoke() {
        final JokeContainerNetworkResponse response = new JokeContainerNetworkResponse(null, "type");
        when(restService.getRandomPersonalisedJoke(any(), any())).thenReturn(Single.just(response));
        return response;
    }

    private Joke givenMapperWillReturnJoke() {
        final Joke joke = new Joke("joke text", "joke id", null);
        when(jokeMapper.map(any())).thenReturn(joke);
        return joke;
    }

    private ListOfJokesContainerNetworkResponse givenRestServiceWillReturnSuccessfulJokes() {
        final ListOfJokesContainerNetworkResponse response = getJokesContainerNetworkResponse();
        when(restService.getListOfJokes(anyInt())).thenReturn(Single.just(response));
        return response;
    }

    private List<Joke> givenMapperWillMapResponseToEntityJoke() {
        final List<Joke> jokes = Collections.emptyList();
        when(collectionMapper.mapList(eq(jokeMapper), any())).thenReturn(jokes);
        return jokes;
    }

    @NonNull
    private ListOfJokesContainerNetworkResponse getJokesContainerNetworkResponse() {
        return new ListOfJokesContainerNetworkResponse(Collections.emptyList(), "type");
    }
}
