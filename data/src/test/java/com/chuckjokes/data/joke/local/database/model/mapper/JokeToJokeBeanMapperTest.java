package com.chuckjokes.data.joke.local.database.model.mapper;

import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JokeToJokeBeanMapperTest {

    private final Joke joke = new Joke("display", "id", "cat");

    private JokeToJokeBeanMapper mapper;

    @Before
    public void setup() {
        mapper = new JokeToJokeBeanMapper();
    }

    @Test
    public void should_return_null_when_joke_is_null() {
        // when
        final JokeBean jokeBean = mapper.map(null);

        // then
        assertThat(jokeBean).isNull();
    }

    @Test
    public void should_map_jokr_correctly() {
        // when
        final JokeBean jokeBean = mapper.map(joke);

        // then
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(jokeBean).isNotNull();
        soft.assertThat(jokeBean.getCategories()).isEqualTo(joke.getCategories());
        soft.assertThat(jokeBean.getDisplayText()).isEqualTo(joke.getDisplayText());
        soft.assertThat(jokeBean.getId()).isEqualTo(joke.getId());
        soft.assertAll();
    }
}