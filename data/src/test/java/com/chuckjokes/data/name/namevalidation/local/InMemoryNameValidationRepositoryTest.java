package com.chuckjokes.data.name.namevalidation.local;

import android.content.res.Resources;

import com.chuckjokes.data.BuildConfig;
import com.chuckjokes.data.R;
import com.chuckjokes.domain.api.util.StringUtil;
import com.chuckjokes.domain.usecase.name.entity.Name;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.model.NameValidationRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.ParameterizedRobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assume.assumeThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(ParameterizedRobolectricTestRunner.class)
@Config(manifest = Config.NONE, constants = BuildConfig.class)
public class InMemoryNameValidationRepositoryTest {

    private static final String NAMES_MUST_START_WITH_A_LETTER = "names_must_start_with_a_letter";
    private static final String MISSING_FIRST_OF_LAST_NAME = "missing_first_or_last";
    private static final String NO_NAME_STRING = "no name string";
    private final Name expectedName;
    private final String validationError;
    private final NameValidationRequest nameValidationRequest;
    private final TestObserver<Name> observer = new TestObserver<>();
    private InMemoryNameValidationRepository inMemoryNameGateway;
    private Resources resources;

    public InMemoryNameValidationRepositoryTest(NameValidationRequest nameValidationRequest,
                                                String validationError,
                                                Name expectedName) {

        this.validationError = validationError;
        this.expectedName = expectedName;
        this.nameValidationRequest = nameValidationRequest;
    }

    @ParameterizedRobolectricTestRunner.Parameters(name = "requests")
    public static Collection<Object[]> requests() {
        final List<Object[]> options = new ArrayList<>();

        // valid requests
        options.add(new Object[]{new NameValidationRequest("valid name"), null, new Name("Valid", "Name")});
        options.add(new Object[]{new NameValidationRequest("  valid   name   "), null, new Name("Valid", "Name")});
        options.add(new Object[]{new NameValidationRequest("  v4110   n433   "), null, new Name("V4110", "N433")});
        options.add(new Object[]{new NameValidationRequest("valid name with multiple surnames"), null,
                new Name("Valid", "Name with multiple surnames")});

        // invalid requests
        options.add(new Object[]{null, NO_NAME_STRING, null});
        options.add(new Object[]{new NameValidationRequest(null), NO_NAME_STRING, null});
        options.add(new Object[]{new NameValidationRequest(StringUtil.EMPTY), NO_NAME_STRING, null});
        options.add(new Object[]{new NameValidationRequest(StringUtil.SPACE), NO_NAME_STRING, null});
        options.add(new Object[]{new NameValidationRequest("   "), NO_NAME_STRING, null});
        options.add(new Object[]{new NameValidationRequest("invalid   "), MISSING_FIRST_OF_LAST_NAME, null});
        options.add(new Object[]{new NameValidationRequest("   invalid"), MISSING_FIRST_OF_LAST_NAME, null});
        options.add(new Object[]{new NameValidationRequest("   invalid   "), MISSING_FIRST_OF_LAST_NAME, null});
        options.add(new Object[]{new NameValidationRequest("invalid_name"), MISSING_FIRST_OF_LAST_NAME, null});
        options.add(new Object[]{new NameValidationRequest("   invalid  3name "), NAMES_MUST_START_WITH_A_LETTER, null});
        options.add(new Object[]{new NameValidationRequest("   454545  343434 "), NAMES_MUST_START_WITH_A_LETTER, null});
        options.add(new Object[]{new NameValidationRequest("   ;r  )s "), NAMES_MUST_START_WITH_A_LETTER, null});

        return options;
    }

    @Before
    public void setup() {
        resources = spy(RuntimeEnvironment.application.getResources());

        inMemoryNameGateway = new InMemoryNameValidationRepository(resources);
    }

    @Test
    public void should_return_validation_failures() {
        // given
        assumeThat(validationError, notNullValue());
        givenResourcesWillReturnStrings();

        // when
        inMemoryNameGateway.getValidatedName(nameValidationRequest).subscribe(observer);

        // then
        observer.assertNoValues();
        observer.assertError(throwable -> {
            return throwable.getMessage().equals(validationError);
        });
    }

    private void givenResourcesWillReturnStrings() {
        when(resources.getString(R.string.missing_personalised_joke_text)).thenReturn(NO_NAME_STRING);
        when(resources.getString(R.string.missing_first_or_last_name_joke_text)).thenReturn(MISSING_FIRST_OF_LAST_NAME);
        when(resources.getString(R.string.names_must_start_with_a_letter)).thenReturn(NAMES_MUST_START_WITH_A_LETTER);
    }

    @Test
    public void should_return_validation_success() {
        // given
        assumeThat(expectedName, notNullValue());
        givenResourcesWillReturnStrings();

        // when
        inMemoryNameGateway.getValidatedName(nameValidationRequest).subscribe(observer);

        // then
        observer.assertNoErrors();
        observer.assertValue(name -> {
            assertThat(name).isNotNull();
            assertThat(name.getLastName()).isEqualTo(expectedName.getLastName());
            assertThat(name.getFirstName()).isEqualTo(expectedName.getFirstName());
            return true;
        });
    }
}
