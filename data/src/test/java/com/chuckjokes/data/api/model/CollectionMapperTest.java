package com.chuckjokes.data.api.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.chuckjokes.domain.api.util.StringUtil.EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CollectionMapperTest {

    private final List<Integer> integersWithNulls = Arrays.asList(null, 2, 4, null, 3);
    private final List<Integer> integers = Arrays.asList(1, 2, 4, 5, 3);
    private final List<Integer> nulls = Arrays.asList(null, null, null);
    private final Integer[] integersArray = new Integer[]{1, 2, 4, 5, 3};

    @Mock
    private Mapper<Integer, String> listItemMapper;

    @Before
    public void setup() {
        givenListItemMapperThatMapsCorrectly();
    }

    private void givenListItemMapperThatMapsCorrectly() {
        when(listItemMapper.map(anyInt())).then(returnCorrectlyMappedString());
    }

    private Answer<String> returnCorrectlyMappedString() {
        return invocation -> {
            final Integer toBeMapped = (Integer) invocation.getArguments()[0];
            return toBeMapped == null ? EMPTY : toBeMapped.toString();
        };
    }

    @Test
    public void return_empty_if_mapped_list_is_null() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, (List<Integer>) null);

        // then
        assertThat(mappingResult).isNotNull().isEmpty();
    }

    @Test
    public void return_empty_if_mapped_list_is_empty() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, new ArrayList<>());

        // then
        assertThat(mappingResult).isEmpty();
    }

    @Test
    public void map_each_list_item_using_listItemMapper() {
        // when
        CollectionMapper.map(listItemMapper, integers);

        // then
        verify(listItemMapper, times(5)).map(anyInt());
    }

    @Test
    public void return_empty_if_mapped_array_is_null() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, (Integer[]) null);

        // then
        assertThat(mappingResult).isNotNull().isEmpty();
    }

    @Test
    public void return_empty_if_mapped_array_is_empty() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, new Integer[0]);

        // then
        assertThat(mappingResult).isEmpty();
    }

    @Test
    public void map_each_array_item_using_listItemMapper() {
        // when
        CollectionMapper.map(listItemMapper, integers);

        // then
        verify(listItemMapper, times(5)).map(anyInt());
    }

    @Test
    public void return_correctly_mapped_list_when_mapped_data_is_correct() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, integersArray);

        // then
        assertThat(mappingResult)
                .isNotNull()
                .hasSize(5)
                .containsExactly("1", "2", "4", "5", "3");
    }

    @Test
    public void return_correctly_mapped_list_when_mapped_data_contains_null_items() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, integersWithNulls);

        // then
        assertThat(mappingResult)
                .isNotNull()
                .hasSize(3)
                .containsExactly("2", "4", "3");
    }

    @Test
    public void return_empty_when_mapped_data_contains_only_null() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, nulls);

        // then
        assertThat(mappingResult).isEmpty();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void return_immutableList() {
        // when
        final List<String> mappingResult = CollectionMapper.map(listItemMapper, integersWithNulls);
        mappingResult.add("should not be allowed to immutable list");
    }
}
