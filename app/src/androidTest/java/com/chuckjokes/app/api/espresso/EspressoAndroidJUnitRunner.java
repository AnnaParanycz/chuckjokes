package com.chuckjokes.app.api.espresso;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

import com.chuckjokes.app.application.presentation.TestApplication;

@SuppressWarnings("WeakerAccess")
public class EspressoAndroidJUnitRunner extends AndroidJUnitRunner {
    @Override
    public Application newApplication(ClassLoader cl, String className, Context context) throws InstantiationException,
            IllegalAccessException, ClassNotFoundException {

        return super.newApplication(cl, TestApplication.class.getName(), context);
    }
}