package com.chuckjokes.app.application.commands.action;

import android.view.inputmethod.EditorInfo;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AndroidKeyboardActionTest {

    private AndroidKeyboardAction androidKeyboardAction;

    @Before
    public void setup() {
        androidKeyboardAction = new AndroidKeyboardAction();
    }

    @Test
    public void should_return_action_go() {
        // when
        final int actionGo = androidKeyboardAction.getActionGo();

        // then
        assertThat(actionGo).isEqualTo(EditorInfo.IME_ACTION_GO);
    }
}
