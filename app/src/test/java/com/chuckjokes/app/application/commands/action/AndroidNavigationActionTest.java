package com.chuckjokes.app.application.commands.action;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AndroidNavigationActionTest {

    private AndroidNavigationAction androidNavigationAction;

    @Before
    public void setup() {
        androidNavigationAction = new AndroidNavigationAction();
    }

    @Test
    public void should_return_up_action() {
        // when
        final int upAction = androidNavigationAction.getUp();

        // then
        assertThat(upAction).isEqualTo(android.R.id.home);
    }
}
