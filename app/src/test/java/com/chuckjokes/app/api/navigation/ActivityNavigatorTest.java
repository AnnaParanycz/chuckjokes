package com.chuckjokes.app.api.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import com.chuckjokes.domain.api.presentation.navigation.Navigatable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE, sdk = Build.VERSION_CODES.LOLLIPOP)
public class ActivityNavigatorTest {

    private TestActivityNavigator testActivityNavigator;

    @Mock
    private Activity activity;

    @Mock
    private Navigatable navigatable;

    @Mock
    private Intent intent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        testActivityNavigator = new TestActivityNavigator(intent);
    }

    @Test
    public void should_do_nothing_when_navigating_without_view() {
        // when
        testActivityNavigator.navigate(null);

        // then
        verifyZeroInteractions(activity);
    }

    @Test
    public void should_do_nothing_when_navigating_without_navigatable() {
        // when
        testActivityNavigator.navigate(navigatable);

        // then
        verifyZeroInteractions(activity);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Test
    public void should_navigate_to_given_intent_when_there_is_activity_navigatable() {
        // given
        givenActivityNavigatable();

        // when
        testActivityNavigator.navigate(navigatable);

        // then
        verify(activity).startActivity(intent);
    }

    private void givenActivityNavigatable() {
        when(navigatable.getNavigatable()).thenReturn(activity);
    }

    private static final class TestActivityNavigator extends ActivityNavigator {

        private final Intent intent;

        private TestActivityNavigator(Intent intent) {
            this.intent = intent;
        }

        @Override
        protected Intent createStartIntent(Activity activity) {
            return intent;
        }
    }
}
