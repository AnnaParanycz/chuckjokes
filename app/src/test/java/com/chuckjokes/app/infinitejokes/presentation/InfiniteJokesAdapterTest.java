package com.chuckjokes.app.infinitejokes.presentation;

import android.os.Build;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chuckjokes.BuildConfig;
import com.chuckjokes.domain.presentation.infinitejokes.InfiniteJokesPresenter;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import static com.chuckjokes.app.infinitejokes.presentation.model.InfiniteJokesViewTypes.JOKE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, manifest = Config.NONE, packageName = "com.chuckjokes.data")
public class InfiniteJokesAdapterTest {

    private static final int PROGRESS_ITEM = 1;
    private static final int POSITION = 3;

    @SuppressWarnings("unchecked")
    private final List<Joke> jokes = Arrays.asList(new Joke("Joke 1", "1", "c1"), new Joke("Joke 2", "1", "c2"),
            new Joke("Joke 3", "3", "c3"), new Joke("Joke 4", "4", "c4"));

    private InfiniteJokesAdapter adapter;
    private ViewGroup viewGroup;

    @Mock
    private InfiniteJokesViewHolder viewHolder;

    @Mock
    private InfiniteJokesPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        viewGroup = spy(new LinearLayout(RuntimeEnvironment.application));
        adapter = spy(new InfiniteJokesAdapter(presenter));
    }

    @Test
    @Ignore(value = "There is an issue with AS 3, Gradle plugin 3, AAPT2 and Robolectric 3.4.2 with linking resources")
    public void should_inflate_view_holder_in_onCreateViewHolder() {
        // when
        final InfiniteJokesViewHolder viewHolder = adapter.onCreateViewHolder(viewGroup, 0);

        // then
        verify(viewGroup, atLeastOnce()).getContext();
        assertThat(viewHolder).isNotNull();
    }

    @Test
    public void should_not_bind_views_on_view_holder_when_there_are_no_jokes() {
        // when
        givenViewHolderStoresJoke();
        adapter.onBindViewHolder(viewHolder, POSITION);

        // then
        verify(viewHolder, never()).setJoke(any());
    }

    private void givenViewHolderStoresJoke() {
        when(viewHolder.getViewType()).thenReturn(JOKE);
    }

    @Test
    public void should_bind_views_on_view_holder_when_there_are_jokes() {
        // given
        givenViewHolderStoresJoke();
        givenListOfJokesIsAdded(jokes);

        // when
        adapter.onBindViewHolder(viewHolder, POSITION);

        // then
        verify(viewHolder).setNumber(String.valueOf(POSITION + 1));
        verify(viewHolder).setJoke(jokes.get(POSITION).getDisplayText());
        verify(viewHolder).setId(jokes.get(POSITION).getId());
    }

    private void givenListOfJokesIsAdded(List<Joke> jokes) {
        adapter.addJokes(jokes);
    }

    @Test
    public void should_return_empty_item_count_if_there_are_no_jokes() {
        // when
        final int size = adapter.getItemCount();

        // then
        assertThat(size).isZero();
    }

    @Test
    public void should_return_correct_item_count_equal_to_sum_of_all_jokes_and_progress_item() {
        // given
        givenListOfJokesIsAdded(jokes);
        givenListOfJokesIsAdded(jokes);

        // when
        final int size = adapter.getItemCount();

        // then
        assertThat(size).isEqualTo(jokes.size() * 2 + PROGRESS_ITEM);
    }

    @Test
    public void should_add_jokes_and_notify_data_set_changed() {
        // when
        adapter.addJokes(jokes);

        // then
        assertThat(adapter.getItemCount()).isEqualTo(jokes.size() + PROGRESS_ITEM);
        verify(adapter).notifyDataSetChanged();
    }
}
