package com.chuckjokes.app.infinitejokes.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import com.chuckjokes.app.infinitejokes.presentation.InfiniteJokesActivity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE, sdk = Build.VERSION_CODES.LOLLIPOP)
public class InfiniteJokesActivityNavigatorTest {

    private InfiniteJokesActivityNavigator infiniteJokesActivityNavigator;

    @Mock
    private Activity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        infiniteJokesActivityNavigator = new InfiniteJokesActivityNavigator(true, "transition");
    }

    @Test
    public void should_return_intent_to_navigate_toInfiniteJokesActivity() {
        // when
        Intent intent = infiniteJokesActivityNavigator.createStartIntent(activity);

        // then
        assertThat(intent).isNotNull();
        assertThat(intent.getComponent().getClassName()).isEqualTo(InfiniteJokesActivity.class.getName());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Test
    public void should_start_activity_with_transition_if_device_supports_it() {
        // when
        infiniteJokesActivityNavigator.startActivity(activity, mock(Intent.class));

        // then

    }
}
