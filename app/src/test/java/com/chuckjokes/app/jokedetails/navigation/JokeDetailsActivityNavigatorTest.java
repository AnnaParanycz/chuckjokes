package com.chuckjokes.app.jokedetails.navigation;

import android.app.Activity;
import android.content.Intent;

import com.chuckjokes.app.jokedetails.presentation.JokeDetailsActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class JokeDetailsActivityNavigatorTest {

    private static final String JOKE_ID = "joke_id";

    private JokeDetailsActivityNavigator jokeDetailsActivityNavigator;

    @Mock
    private Activity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        jokeDetailsActivityNavigator = new JokeDetailsActivityNavigator();
    }


    @Test
    public void should_create_intent_to_JokeDetailsActivity() {
        // when
        jokeDetailsActivityNavigator.id(JOKE_ID);
        Intent intent = jokeDetailsActivityNavigator.createStartIntent(activity);

        // then
        assertThat(intent).isNotNull();
        assertThat(intent.getComponent().getClassName()).isEqualTo(JokeDetailsActivity.class.getName());
    }
}


