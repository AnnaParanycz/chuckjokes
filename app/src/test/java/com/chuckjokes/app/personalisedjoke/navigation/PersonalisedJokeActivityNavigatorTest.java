package com.chuckjokes.app.personalisedjoke.navigation;

import android.app.Activity;
import android.content.Intent;

import com.chuckjokes.app.personalisedjoke.presentation.PersonalisedJokeActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class PersonalisedJokeActivityNavigatorTest {

    private PersonalisedJokeActivityNavigator personalisedJokeActivityNavigator;

    @Mock
    private Activity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        personalisedJokeActivityNavigator = new PersonalisedJokeActivityNavigator();
    }

    @Test
    public void should_create_intent_to_PersonalisedJokeActivity() {
        // when
        Intent intent = personalisedJokeActivityNavigator.createStartIntent(activity);

        // then
        assertThat(intent).isNotNull();
        assertThat(intent.getComponent().getClassName()).isEqualTo(PersonalisedJokeActivity.class.getName());
    }
}
