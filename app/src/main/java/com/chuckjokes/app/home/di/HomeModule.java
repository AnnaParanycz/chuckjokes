package com.chuckjokes.app.home.di;

import android.content.res.Resources;
import android.net.Uri;

import com.chuckjokes.R;
import com.chuckjokes.app.api.di.qualifier.Names;
import com.chuckjokes.app.api.di.qualifier.TransitionsSupported;
import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.infinitejokes.navigation.InfiniteJokesActivityNavigator;
import com.chuckjokes.app.personalisedjoke.navigation.PersonalisedJokeActivityNavigator;
import com.chuckjokes.domain.presentation.home.HomePresenter;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.GetRandomJokeInteractor;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
class HomeModule {

    @Provides
    @PerActivity
    HomePresenter homePresenter(InfiniteJokesActivityNavigator getInfiniteJokesNavigator,
                                PersonalisedJokeActivityNavigator getPersonalisedJokeNavigator,
                                GetRandomJokeInteractor getRandomJokeInteractor) {
        return new HomePresenter(getInfiniteJokesNavigator, getPersonalisedJokeNavigator, getRandomJokeInteractor);
    }

    @Provides
    @PerActivity
    PersonalisedJokeActivityNavigator personalisedJokeActivityNavigator() {
        return new PersonalisedJokeActivityNavigator();
    }

    @Provides
    @PerActivity
    InfiniteJokesActivityNavigator infiniteJokesActivityNavigator(
            @Named(Names.TO_INFINITE_SCREEN_TRANSITION_NAME) String transitionName,
            @TransitionsSupported boolean transitionsSupported) {
        return new InfiniteJokesActivityNavigator(transitionsSupported, transitionName);
    }

    @Provides
    @Named(Names.INFINITE_JOKES_PICTURE)
    Uri infiniteJokesPicture() {
        return Uri.parse("file:///android_asset/chuck_norris_3.jpg");
    }

    @Provides
    @Named(Names.RANDOM_JOKE_PICTURE)
    Uri randomJokePicture() {
        return Uri.parse("file:///android_asset/cowboy.jpg");
    }

    @Provides
    @Named(Names.PERSONALISED_JOKE_PICTURE)
    Uri personalisedJokePicture() {
        return Uri.parse("file:///android_asset/sheriff.jpg");
    }

    @Provides
    @Named(Names.TO_INFINITE_SCREEN_TRANSITION_NAME)
    String infiniteScreenTransitionName(Resources resources) {
        return resources.getString(R.string.to_infinite_jokes_picture_transition);
    }
}
