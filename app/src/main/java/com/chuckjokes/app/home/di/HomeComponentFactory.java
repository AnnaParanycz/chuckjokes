package com.chuckjokes.app.home.di;

import android.app.Activity;

import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseApplication;
import com.chuckjokes.app.home.presentation.HomeActivity;

public class HomeComponentFactory implements ActivityComponentFactory<HomeComponent> {

    @Override
    public HomeComponent create(Activity activity) {
        return DaggerHomeComponent.builder()
                .homeModule(new HomeModule())
                .applicationComponent(BaseApplication.get(activity).getApplicationComponent())
                .build();
    }

    @Override
    public void inject(HomeComponent component, Activity activity) {
        component.inject((HomeActivity) activity);
    }
}
