package com.chuckjokes.app.home.presentation;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chuckjokes.R;
import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.api.di.qualifier.Names;
import com.chuckjokes.app.application.presentation.BaseActivity;
import com.chuckjokes.app.home.di.HomeComponent;
import com.chuckjokes.app.home.di.HomeComponentFactory;
import com.chuckjokes.app.widgets.dialog.DismissableActionDialogFragment;
import com.chuckjokes.app.widgets.dialog.DismissableDialogListener;
import com.chuckjokes.domain.presentation.home.HomePresenter;
import com.chuckjokes.domain.presentation.home.model.HomeView;

import javax.inject.Inject;
import javax.inject.Named;

public class HomeActivity extends BaseActivity<HomeComponent> implements HomeView, DismissableDialogListener {

    private static final float SEMI_TRANSPARENT = 0.5f;
    private static final float SOLID = 1f;

    @SuppressWarnings("WeakerAccess")
    @Inject
    @Named(Names.INFINITE_JOKES_PICTURE)
    Uri infiniteJokesPictureLocation;

    @SuppressWarnings("WeakerAccess")
    @Inject
    @Named(Names.RANDOM_JOKE_PICTURE)
    Uri randomJokePictureLocation;

    @SuppressWarnings("WeakerAccess")
    @Inject
    @Named(Names.PERSONALISED_JOKE_PICTURE)
    Uri personalisedJokePictureLocation;

    @SuppressWarnings("WeakerAccess")
    @Inject
    HomePresenter presenter;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.activity_home_random_joke_container)
    View randomJokeView;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.activity_home_random_joke_button)
    ImageView randomJokeButton;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.activity_home_infinite_jokes_button)
    ImageView infiniteJokesButton;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.activity_home_personalised_joke_button)
    ImageView personalisedJokeButton;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.activity_home_random_joke_loading)
    View randomJokeProgressBar;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.activity_home_infinite_jokes_container)
    void onInfiniteJokesButtonClicked() {
        presenter.onInfiniteJokesButtonClicked();
    }

    @OnClick(R.id.activity_home_random_joke_container)
    void onRandomJokeButtonClicked() {
        presenter.onRandomJokeButtonClicked();
    }

    @OnClick(R.id.activity_home_personalised_joke_container)
    void onPersonalisedJokeButtonClicked() {
        presenter.onPersonalisedJokeButtonClicked();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @NonNull
    @Override
    protected ActivityComponentFactory<HomeComponent> createActivityComponentFactory() {
        return new HomeComponentFactory();
    }

    @Override
    public void initUi() {
        setContentView(R.layout.activity_home);
        initToolbar();
        initButtons();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        setTitle(R.string.chuck_humour);
    }

    private void initButtons() {
        loadPicture(infiniteJokesPictureLocation, infiniteJokesButton);
        loadPicture(randomJokePictureLocation, randomJokeButton);
        loadPicture(personalisedJokePictureLocation, personalisedJokeButton);
    }

    private void loadPicture(Uri uri, ImageView imageView) {
        Glide.with(this).load(uri).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(imageView);
    }

    @Override
    public void showLoadingRandomJoke() {
        randomJokeProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void disableRandomJokeButton() {
        randomJokeView.setEnabled(false);
        randomJokeButton.setAlpha(SEMI_TRANSPARENT);
    }

    @Override
    public void enableRandomJokeButton() {
        randomJokeView.setEnabled(true);
        randomJokeButton.setAlpha(SOLID);
    }

    @Override
    public void hideLoadingRandomJoke() {
        randomJokeProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void displayRandomJokeDialog(String tag, String displayText) {
        DismissableActionDialogFragment.newInstance(tag)
                .message(displayText)
                .show(getSupportFragmentManager());
    }

    @Override
    public void displayRandomJokeFailedDialog(String tag) {
        DismissableActionDialogFragment.newInstance(tag)
                .positiveButtonText(R.string.random_joke_failed_dialog_positive_button_text)
                .title(R.string.random_joke_failed_dialog_title)
                .message(R.string.random_joke_failed_message)
                .show(getSupportFragmentManager());
    }

    @Override
    public void onDialogDismissed(String tag) {
        presenter.onDialogDismissed(tag);
    }
}
