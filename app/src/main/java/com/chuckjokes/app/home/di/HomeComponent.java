package com.chuckjokes.app.home.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.home.presentation.HomeActivity;
import com.chuckjokes.app.personalisedjoke.navigation.PersonalisedJokeActivityNavigator;
import com.chuckjokes.app.application.di.ApplicationComponent;
import com.chuckjokes.app.infinitejokes.navigation.InfiniteJokesActivityNavigator;

import dagger.Component;

@PerActivity
@Component(
        modules = HomeModule.class,
        dependencies = ApplicationComponent.class
)
public interface HomeComponent {

    void inject(HomeActivity homeActivity);

    PersonalisedJokeActivityNavigator personalisedJokeActivityNavigator();

    InfiniteJokesActivityNavigator infiniteJokesActivityNavigator();
}
