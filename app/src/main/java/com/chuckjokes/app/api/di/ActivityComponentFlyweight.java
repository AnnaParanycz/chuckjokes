package com.chuckjokes.app.api.di;

import android.support.annotation.NonNull;

import com.chuckjokes.app.application.presentation.BaseActivity;

public class ActivityComponentFlyweight<T> {

    private final ActivityComponentFactory<T> activityComponentFactory;

    public ActivityComponentFlyweight(@NonNull ActivityComponentFactory<T> activityComponentFactory) {
        this.activityComponentFactory = activityComponentFactory;
    }

    public T create(@NonNull BaseActivity<T> activity) {
        final T component = getActivityComponent(activity);
        activityComponentFactory.inject(component, activity);
        return component;
    }

    @SuppressWarnings("unchecked")
    private T getActivityComponent(@NonNull BaseActivity<T> activity) {
        final T currentActivityComponent = activity.getActivityComponent();
        if (currentActivityComponent != null) {
            return currentActivityComponent;
        }

        final T savedHomeComponent = (T) activity.getLastCustomNonConfigurationInstance();
        if (savedHomeComponent != null) {
            return savedHomeComponent;
        }

        return activityComponentFactory.create(activity);
    }
}
