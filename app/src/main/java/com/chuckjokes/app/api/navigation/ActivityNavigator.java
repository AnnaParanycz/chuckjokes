package com.chuckjokes.app.api.navigation;

import android.app.Activity;
import android.content.Intent;
import com.chuckjokes.domain.api.presentation.navigation.Navigatable;
import com.chuckjokes.domain.api.presentation.navigation.Navigator;

public abstract class ActivityNavigator implements Navigator {

    @Override
    public void navigate(Navigatable navigatable) {
        if (isValidNavigatable(navigatable)) {
            Activity activity = (Activity) navigatable.getNavigatable();
            startActivity(activity, createStartIntent(activity));
        }
    }

    protected void startActivity(Activity activity, Intent intent) {
        activity.startActivity(intent);
    }

    protected abstract Intent createStartIntent(Activity activity);

    private boolean isValidNavigatable(Navigatable navigatable) {
        return navigatable != null
                && navigatable.getNavigatable() != null
                && navigatable.getNavigatable() instanceof Activity;
    }
}
