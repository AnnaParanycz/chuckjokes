package com.chuckjokes.app.api.di;

import android.app.Activity;

public interface ActivityComponentFactory<T> {

    T create(Activity activity);

    void inject(T component, Activity activity);
}
