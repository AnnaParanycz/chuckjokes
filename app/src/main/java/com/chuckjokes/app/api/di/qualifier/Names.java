package com.chuckjokes.app.api.di.qualifier;

public class Names {

    public static final String TO_INFINITE_SCREEN_TRANSITION_NAME = "to_infinite_screen_transition_name";
    public static final String INFINITE_JOKES_PICTURE = "infinite_jokes_picture";
    public static final String RANDOM_JOKE_PICTURE = "random_joke_picture";
    public static final String PERSONALISED_JOKE_PICTURE = "personalised_joke_picture";
}
