package com.chuckjokes.app.application.di;

import android.content.res.Resources;
import android.net.ConnectivityManager;

import com.chuckjokes.app.api.di.qualifier.TransitionsSupported;
import com.chuckjokes.app.api.di.scope.PerApplication;
import com.chuckjokes.app.application.presentation.BaseApplication;
import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.api.presentation.usercommands.KeyboardAction;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.GetJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.GetListOfJokesInteractor;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.GetRandomJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.GetRandomPersonalisedJokeInteractor;

import dagger.Component;

@PerApplication
@Component(
        modules = {
                ApplicationModule.class,
                ApplicationDataModule.class
        }
)
public interface ApplicationComponent {

    void inject(BaseApplication baseApplication);

    Resources resources();

    KeyboardAction keyboardAction();

    @TransitionsSupported
    boolean transitionsSupported();

    NavigationAction navigationAction();

    GetListOfJokesInteractor getListOfJokesInteractor();

    GetRandomJokeInteractor getRandomJokeInteractor();

    GetJokeInteractor getJokeInteractor();

    GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor();

    ConnectivityManager connectivityManager();
}
