package com.chuckjokes.app.application.di;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;

import com.chuckjokes.app.api.di.qualifier.ApplicationContext;
import com.chuckjokes.app.api.di.scope.PerApplication;
import com.chuckjokes.app.api.rx.AndroidSchedulerProvider;
import com.chuckjokes.data.api.model.Mapper;
import com.chuckjokes.data.joke.JokeRepositoryStrategy;
import com.chuckjokes.data.joke.local.LocalJokeRepository;
import com.chuckjokes.data.joke.local.database.LocalDatabaseJokeRepository;
import com.chuckjokes.data.joke.local.database.model.JokeBean;
import com.chuckjokes.data.joke.local.database.model.mapper.JokeBeanMapper;
import com.chuckjokes.data.joke.local.database.model.mapper.JokeToJokeBeanMapper;
import com.chuckjokes.data.joke.local.database.service.JokeDatabaseService;
import com.chuckjokes.data.joke.local.database.service.RealmJokeDatabaseService;
import com.chuckjokes.data.joke.remote.RemoteJokeRepository;
import com.chuckjokes.data.joke.remote.model.JokeNetworkResponse;
import com.chuckjokes.data.joke.remote.model.mapper.JokeMapper;
import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestService;
import com.chuckjokes.data.joke.remote.service.retrofit.RetrofitRemoteRestServiceFactory;
import com.chuckjokes.data.name.namevalidation.local.InMemoryNameValidationRepository;
import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.GetJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.GetListOfJokesInteractor;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.GetRandomJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.GetRandomPersonalisedJokeInteractor;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmConfiguration;

@Module
public class ApplicationDataModule {

    @PerApplication
    @Provides
    RetrofitRemoteRestService retrofitRemoteRestService() {
        return new RetrofitRemoteRestServiceFactory().create();
    }

    @PerApplication
    @Provides
    SchedulerProvider schedulerProvider() {
        return new AndroidSchedulerProvider();
    }

    @Provides
    @PerApplication
    JokeRepository jokeRepository(RemoteJokeRepository remoteJokeRepository,
                                  LocalJokeRepository localJokeRepository,
                                  ConnectivityManager connectivityManager) {
        return new JokeRepositoryStrategy(remoteJokeRepository, localJokeRepository, connectivityManager);
    }

    @Provides
    @PerApplication
    LocalJokeRepository localJokeRepository(JokeDatabaseService jokeDatabaseService,
                                            Mapper<Joke, JokeBean> jokeBeanMapper,
                                            Mapper<JokeBean, Joke> jokeMapper) {
        return new LocalDatabaseJokeRepository(jokeDatabaseService, jokeBeanMapper, jokeMapper);
    }

    @Provides
    @PerApplication
    JokeDatabaseService jokeDatabaseService(Provider<Realm> realm) {
        return new RealmJokeDatabaseService(realm, Schedulers.single());
    }

    @Provides
    Mapper<JokeBean, Joke> beanJokeMapper() {
        return new JokeBeanMapper();
    }

    @Provides
    Mapper<Joke, JokeBean> jokeToBeanMapper() {
        return new JokeToJokeBeanMapper();
    }

    @Provides
    @PerApplication
    RemoteJokeRepository remoteJokeRepository(Mapper<JokeNetworkResponse, Joke> jokeMapper,
                                              RetrofitRemoteRestService retrofitRemoteRestService) {
        return new RemoteJokeRepository(retrofitRemoteRestService, jokeMapper);
    }

    @Provides
    GetListOfJokesInteractor getListOfJokesInteractor(SchedulerProvider schedulerProvider,
                                                      JokeRepository listOfJokesRepository) {
        return new GetListOfJokesInteractor(schedulerProvider, listOfJokesRepository);
    }

    @Provides
    GetRandomJokeInteractor getRandomJokeInteractor(JokeRepository jokeRepository,
                                                    SchedulerProvider schedulerProvider) {
        return new GetRandomJokeInteractor(jokeRepository, schedulerProvider);
    }

    @Provides
    GetJokeInteractor getJokeInteractor(JokeRepository jokeRepository,
                                        SchedulerProvider schedulerProvider) {
        return new GetJokeInteractor(jokeRepository, schedulerProvider);
    }

    @Provides
    GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor(JokeRepository jokeRepository,
                                                                            SchedulerProvider schedulerProvider,
                                                                            NameValidationRepository nameValidationRepository) {
        return new GetRandomPersonalisedJokeInteractor(jokeRepository, schedulerProvider, nameValidationRepository);
    }

    @Provides
    Mapper<JokeNetworkResponse, Joke> jokeMapper() {
        return new JokeMapper();
    }

    @Provides
    NameValidationRepository nameValidationRepository(Resources resources) {
        return new InMemoryNameValidationRepository(resources);
    }

    @Provides
    @PerApplication
    Realm realm(@ApplicationContext Context context) {
        Realm.init(context);
        return Realm.getInstance(new RealmConfiguration.Builder().build());
    }
}
