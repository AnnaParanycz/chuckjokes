package com.chuckjokes.app.application.presentation;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.api.di.ActivityComponentFlyweight;
import com.chuckjokes.domain.api.presentation.navigation.Navigatable;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Base activity for shared components
 */
public abstract class BaseActivity<T> extends AppCompatActivity implements Navigatable {

    private T activityComponent;
    private Unbinder unbinder;

    @Override
    @CallSuper
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = new ActivityComponentFlyweight<>(createActivityComponentFactory()).create(this);
    }

    @NonNull
    protected abstract ActivityComponentFactory<T> createActivityComponentFactory();

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        bind();
    }

    private void bind() {
        unbinder = ButterKnife.bind(this);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        bind();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        bind();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return getActivityComponent();
    }

    public T getActivityComponent() {
        return activityComponent;
    }

    @Override
    public Object getNavigatable() {
        return this;
    }
}
