package com.chuckjokes.app.application.commands.action;

import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;

public class AndroidNavigationAction extends NavigationAction {

    @Override
    public int getUp() {
        return android.R.id.home;
    }
}
