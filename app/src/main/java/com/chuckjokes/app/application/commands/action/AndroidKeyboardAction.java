package com.chuckjokes.app.application.commands.action;

import android.view.inputmethod.EditorInfo;

import com.chuckjokes.domain.api.presentation.usercommands.KeyboardAction;

public class AndroidKeyboardAction extends KeyboardAction {

    @Override
    protected int getActionGo() {
        return EditorInfo.IME_ACTION_GO;
    }
}
