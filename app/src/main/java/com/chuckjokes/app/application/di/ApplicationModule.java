package com.chuckjokes.app.application.di;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;

import com.chuckjokes.app.api.di.qualifier.ApplicationContext;
import com.chuckjokes.app.api.di.qualifier.TransitionsSupported;
import com.chuckjokes.app.api.di.scope.PerApplication;
import com.chuckjokes.app.application.commands.action.AndroidKeyboardAction;
import com.chuckjokes.app.application.commands.action.AndroidNavigationAction;
import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.api.presentation.usercommands.KeyboardAction;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @PerApplication
    Application application() {
        return application;
    }

    @Provides
    @PerApplication
    @ApplicationContext
    Context context() {
        return application;
    }

    @Provides
    @PerApplication
    Resources resources(@ApplicationContext Context context) {
        return context.getResources();
    }

    @Provides
    @PerApplication
    KeyboardAction keyboardAction() {
        return new AndroidKeyboardAction();
    }

    @Provides
    @PerApplication
    NavigationAction navigationAction() {
        return new AndroidNavigationAction();
    }

    @Provides
    @TransitionsSupported
    boolean transitionsSupported() {
        return Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT;
    }

    @Provides
    ConnectivityManager connectivityManager(@ApplicationContext Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
