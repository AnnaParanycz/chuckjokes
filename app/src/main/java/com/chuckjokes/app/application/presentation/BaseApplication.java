package com.chuckjokes.app.application.presentation;

import android.app.Application;
import android.content.Context;

import com.chuckjokes.app.application.di.ApplicationComponent;
import com.chuckjokes.app.application.di.ApplicationDataModule;
import com.chuckjokes.app.application.di.ApplicationModule;
import com.chuckjokes.app.application.di.DaggerApplicationComponent;
import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.api.presentation.usercommands.KeyboardAction;

/**
 * Application base
 */
public class BaseApplication extends Application {

    private ApplicationComponent applicationComponent;

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = createApplicationComponent();
        applicationComponent.inject(this);

        KeyboardAction.setInstance(applicationComponent.keyboardAction());
        NavigationAction.setInstance(applicationComponent.navigationAction());
    }

    @SuppressWarnings("WeakerAccess")
    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .applicationDataModule(new ApplicationDataModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
