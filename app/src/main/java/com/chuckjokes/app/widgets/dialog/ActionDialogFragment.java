package com.chuckjokes.app.widgets.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.MESSAGE;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.MESSAGE_ID;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.NEGATIVE_BUTTON_TEXT;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.NEGATIVE_BUTTON_TEXT_ID;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.NEUTRAL_BUTTON_TEXT;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.NEUTRAL_BUTTON_TEXT_ID;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.POSITIVE_BUTTON_TEXT;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.POSITIVE_BUTTON_TEXT_ID;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.TITLE;
import static com.chuckjokes.app.widgets.dialog.DialogFragmentConstants.TITLE_ID;

public abstract class ActionDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Bundle bundle = getArguments();
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        addTitle(bundle, builder);
        addMessage(bundle, builder);
        addPositiveButton(bundle, builder);
        addNegativeButton(bundle, builder);
        addNeutralButton(bundle, builder);

        return builder.create();
    }

    private void addTitle(Bundle bundle, AlertDialog.Builder builder) {
        final String title = bundle.getString(TITLE);
        final int titleId = bundle.getInt(TITLE_ID);

        if (title != null) {
            builder.setTitle(title);
        } else if (titleId != View.NO_ID) {
            builder.setTitle(titleId);
        }
    }

    private void addMessage(Bundle bundle, AlertDialog.Builder builder) {
        final String message = bundle.getString(MESSAGE);
        final int messageId = bundle.getInt(MESSAGE_ID);

        if (message != null) {
            builder.setMessage(message);
        } else {
            builder.setMessage(messageId);
        }
    }

    private void addPositiveButton(Bundle bundle, AlertDialog.Builder builder) {
        final DialogInterface.OnClickListener positiveButtonListener = createPositiveButtonListener();
        final String positiveButtonText = bundle.getString(POSITIVE_BUTTON_TEXT);
        final int positiveButtonTextId = bundle.getInt(POSITIVE_BUTTON_TEXT_ID);

        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, positiveButtonListener);
        } else {
            builder.setPositiveButton(positiveButtonTextId, positiveButtonListener);
        }
    }

    private void addNegativeButton(Bundle bundle, AlertDialog.Builder builder) {
        final DialogInterface.OnClickListener negativeButtonListener = createNegativeButtonListener();
        if (negativeButtonListener == null) {
            return;
        }

        final String negativeButtonText = bundle.getString(NEGATIVE_BUTTON_TEXT);
        final int negativeButtonTextId = bundle.getInt(NEGATIVE_BUTTON_TEXT_ID);

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, negativeButtonListener);
        } else {
            builder.setNegativeButton(negativeButtonTextId, negativeButtonListener);
        }
    }

    private void addNeutralButton(Bundle bundle, AlertDialog.Builder builder) {
        final DialogInterface.OnClickListener neutralButtonListener = createNeutralButtonListener();
        if (neutralButtonListener == null) {
            return;
        }

        final String neutralButtonText = bundle.getString(NEUTRAL_BUTTON_TEXT);
        final int neutralButtonTextId = bundle.getInt(NEUTRAL_BUTTON_TEXT_ID);

        if (neutralButtonText != null) {
            builder.setNeutralButton(neutralButtonText, neutralButtonListener);
        } else if (neutralButtonTextId != View.NO_ID) {
            builder.setNeutralButton(neutralButtonTextId, neutralButtonListener);
        }
    }

    @NonNull
    abstract DialogInterface.OnClickListener createPositiveButtonListener();

    @SuppressWarnings({"WeakerAccess", "SameReturnValue"})
    @Nullable
    DialogInterface.OnClickListener createNegativeButtonListener() {
        return null;
    }

    @SuppressWarnings({"WeakerAccess", "SameReturnValue"})
    @Nullable
    DialogInterface.OnClickListener createNeutralButtonListener() {
        return null;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        final Fragment targetFragment = getTargetFragment();
        if (targetFragment != null && targetFragment instanceof DismissableDialogListener) {
            ((DismissableDialogListener) targetFragment).onDialogDismissed(getTag());
            return;
        }

        final Context context = getContext();
        if (context != null && context instanceof DismissableDialogListener) {
            ((DismissableDialogListener) context).onDialogDismissed(getTag());
        }
    }
}
