package com.chuckjokes.app.widgets.dialog;

public interface DismissableDialogListener {

    void onDialogDismissed(String tag);
}
