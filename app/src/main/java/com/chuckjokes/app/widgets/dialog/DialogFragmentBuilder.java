package com.chuckjokes.app.widgets.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import dagger.internal.Preconditions;

@SuppressWarnings({"SameParameterValue", "unused"})
public class DialogFragmentBuilder {

    private final DialogFragment dialogFragment;
    private final String tag;

    private String positiveButtonText;
    private String negativeButtonText;
    private String neutralButtonText;
    private String message;
    private String title;

    private int positiveButtonTextId = View.NO_ID;
    private int negativeButtonTextId = View.NO_ID;
    private int neutralButtonTextId = View.NO_ID;
    private int messageId = View.NO_ID;
    private int titleId = View.NO_ID;

    DialogFragmentBuilder(@NonNull String tag, DialogFragment dialogFragment) {
        this.dialogFragment = Preconditions.checkNotNull(dialogFragment);
        this.tag = Preconditions.checkNotNull(tag);
    }

    public DialogFragmentBuilder message(String message) {
        this.message = message;
        this.messageId = View.NO_ID;
        return this;
    }

    public DialogFragmentBuilder message(int messageId) {
        this.messageId = messageId;
        this.message = null;
        return this;
    }

    public DialogFragmentBuilder title(String title) {
        this.title = title;
        this.titleId = View.NO_ID;
        return this;
    }

    public DialogFragmentBuilder title(int titleId) {
        this.titleId = titleId;
        this.title = null;
        return this;
    }

    public DialogFragmentBuilder positiveButtonText(int buttonTextId) {
        this.positiveButtonTextId = buttonTextId;
        this.positiveButtonText = null;
        return this;
    }

    public DialogFragmentBuilder positiveButtonText(String buttonText) {
        this.positiveButtonText = buttonText;
        this.positiveButtonTextId = View.NO_ID;
        return this;
    }

    public DialogFragmentBuilder negativeButtonText(int buttonTextId) {
        this.negativeButtonTextId = buttonTextId;
        this.negativeButtonText = null;
        return this;
    }

    public DialogFragmentBuilder negativeButtonText(String buttonText) {
        this.negativeButtonText = buttonText;
        this.negativeButtonTextId = View.NO_ID;
        return this;
    }

    public DialogFragmentBuilder neutralButtonText(int buttonTextId) {
        this.neutralButtonTextId = buttonTextId;
        this.neutralButtonText = null;
        return this;
    }

    public DialogFragmentBuilder neutralButtonText(String buttonText) {
        this.neutralButtonText = buttonText;
        this.neutralButtonTextId = View.NO_ID;
        return this;
    }

    public void show(FragmentManager fragmentManager) {
        dialogFragment.setArguments(createArguments());
        dialogFragment.show(fragmentManager, tag);
    }

    private Bundle createArguments() {
        final Bundle bundle = new Bundle();

        bundle.putInt(DialogFragmentConstants.POSITIVE_BUTTON_TEXT_ID, positiveButtonTextId == View.NO_ID ? android.R.string.ok : positiveButtonTextId);
        bundle.putInt(DialogFragmentConstants.NEGATIVE_BUTTON_TEXT_ID, negativeButtonTextId == View.NO_ID ? android.R.string.cancel : neutralButtonTextId);
        bundle.putInt(DialogFragmentConstants.NEUTRAL_BUTTON_TEXT_ID, neutralButtonTextId);
        bundle.putInt(DialogFragmentConstants.MESSAGE_ID, messageId);
        bundle.putInt(DialogFragmentConstants.TITLE_ID, titleId);

        bundle.putString(DialogFragmentConstants.POSITIVE_BUTTON_TEXT, positiveButtonText);
        bundle.putString(DialogFragmentConstants.NEGATIVE_BUTTON_TEXT, negativeButtonText);
        bundle.putString(DialogFragmentConstants.NEUTRAL_BUTTON_TEXT, negativeButtonText);
        bundle.putString(DialogFragmentConstants.MESSAGE, message);
        bundle.putString(DialogFragmentConstants.TITLE, title);

        return bundle;
    }

    public void show(Fragment fragment, FragmentManager fragmentManager) {
        dialogFragment.setTargetFragment(fragment, tag.hashCode());
        dialogFragment.setArguments(createArguments());
        dialogFragment.show(fragmentManager, tag);
    }
}
