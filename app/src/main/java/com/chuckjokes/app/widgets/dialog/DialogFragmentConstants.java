package com.chuckjokes.app.widgets.dialog;

class DialogFragmentConstants {

    static final String POSITIVE_BUTTON_TEXT_ID = "positive_button_text_id";
    static final String NEGATIVE_BUTTON_TEXT_ID = "negative_button_text_id";
    static final String NEUTRAL_BUTTON_TEXT_ID = "neutral_button_text_id";
    static final String POSITIVE_BUTTON_TEXT = "positive_button_text";
    static final String NEGATIVE_BUTTON_TEXT = "negative_button_text";
    static final String NEUTRAL_BUTTON_TEXT = "neutral_button_text";
    static final String MESSAGE_ID = "message_id";
    static final String TITLE_ID = "title_id";
    static final String MESSAGE = "message";
    static final String TITLE = "title";
}
