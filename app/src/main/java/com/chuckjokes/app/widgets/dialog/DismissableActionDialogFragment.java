package com.chuckjokes.app.widgets.dialog;

import android.content.DialogInterface;
import android.support.annotation.NonNull;

public class DismissableActionDialogFragment extends ActionDialogFragment {

    public static DialogFragmentBuilder newInstance(String tag) {
        return new DialogFragmentBuilder(tag, new DismissableActionDialogFragment());
    }

    @NonNull
    DialogInterface.OnClickListener createPositiveButtonListener() {
        return new DismissingPositiveButtonClickListener();
    }

    private class DismissingPositiveButtonClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dismiss();
        }
    }
}
