package com.chuckjokes.app.infinitejokes.di;

import android.app.Activity;

import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.infinitejokes.presentation.InfiniteJokesActivity;
import com.chuckjokes.app.application.presentation.BaseApplication;

public class InfiniteJokesComponentFactory implements ActivityComponentFactory<InfiniteJokesComponent> {
    @Override
    public InfiniteJokesComponent create(Activity activity) {
        return DaggerInfiniteJokesComponent.builder()
                .applicationComponent(BaseApplication.get(activity).getApplicationComponent())
                .infiniteJokesModule(new InfiniteJokesModule())
                .build();
    }

    @Override
    public void inject(InfiniteJokesComponent component, Activity activity) {
        component.inject((InfiniteJokesActivity) activity);
    }
}
