package com.chuckjokes.app.infinitejokes.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.application.di.ApplicationComponent;
import com.chuckjokes.app.infinitejokes.presentation.InfiniteJokesActivity;

import dagger.Component;

@PerActivity
@Component(
        modules = InfiniteJokesModule.class,
        dependencies = ApplicationComponent.class
)
public interface InfiniteJokesComponent {

    void inject(InfiniteJokesActivity activity);
}
