package com.chuckjokes.app.infinitejokes.presentation;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chuckjokes.R;
import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseActivity;
import com.chuckjokes.app.infinitejokes.di.InfiniteJokesComponent;
import com.chuckjokes.app.infinitejokes.di.InfiniteJokesComponentFactory;
import com.chuckjokes.domain.presentation.infinitejokes.InfiniteJokesPresenter;
import com.chuckjokes.domain.presentation.infinitejokes.model.InfiniteJokesView;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@SuppressWarnings("WeakerAccess")
public class InfiniteJokesActivity extends BaseActivity<InfiniteJokesComponent> implements InfiniteJokesView {

    @Inject
    InfiniteJokesPresenter presenter;

    @Inject
    InfiniteJokesAdapter adapter;

    @Inject
    Uri toolbarPictureLocation;

    @BindView(R.id.activity_infinite_jokes_recycler_view)
    RecyclerView jokesList;

    @BindView(R.id.activity_infinite_jokes_no_jokes_available)
    View noJokesError;

    @BindView(R.id.activity_infinite_jokes_loading)
    View loading;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_infinite_jokes_error)
    View loadingError;

    @BindView(R.id.activity_infinite_jokes_collapsing_image)
    ImageView toolbarPicture;

    private ListOfJokesScrollListener listOfJokesScrollListener;

    @OnClick(R.id.activity_infinite_jokes_error)
    void onRetryToLoadJokes() {
        presenter.onRetryToLoadJokes();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        if (jokesList != null) {
            jokesList.removeOnScrollListener(listOfJokesScrollListener);
        }
    }

    @NonNull
    @Override
    protected ActivityComponentFactory<InfiniteJokesComponent> createActivityComponentFactory() {
        return new InfiniteJokesComponentFactory();
    }

    @Override
    public void initUi() {
        setContentView(R.layout.activity_infinite_jokes);
        initToolbar();
        initAdapter();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        setTitle(R.string.infinite_jokes);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Glide.with(this).load(toolbarPictureLocation)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(toolbarPicture);
    }

    private void initAdapter() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        jokesList.setHasFixedSize(false);
        jokesList.setLayoutManager(layoutManager);
        jokesList.setItemAnimator(new DefaultItemAnimator());
        jokesList.setAdapter(adapter);

        listOfJokesScrollListener = new ListOfJokesScrollListener(layoutManager, () -> presenter.onJokeListBottomReached());
        jokesList.addOnScrollListener(listOfJokesScrollListener);
    }

    @Override
    public void addJokes(List<Joke> jokes) {
        adapter.addJokes(jokes);
    }

    @Override
    public void showError() {
        loading.setVisibility(View.GONE);
        jokesList.setVisibility(View.GONE);
        noJokesError.setVisibility(View.GONE);
        loadingError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingScreen() {
        loading.setVisibility(View.VISIBLE);
        jokesList.setVisibility(View.GONE);
        loadingError.setVisibility(View.GONE);
        noJokesError.setVisibility(View.GONE);
    }

    @Override
    public void showLoaded() {
        loading.setVisibility(View.GONE);
        jokesList.setVisibility(View.VISIBLE);
        loadingError.setVisibility(View.GONE);
        noJokesError.setVisibility(View.GONE);
    }

    @Override
    public boolean hasJokes() {
        return adapter.hasJokes();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        presenter.onOptionsItemSelected(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
        super.onBackPressed();
    }

    @Override
    public void showNoJokesAvailableError() {
        loading.setVisibility(View.GONE);
        jokesList.setVisibility(View.GONE);
        loadingError.setVisibility(View.GONE);
        noJokesError.setVisibility(View.VISIBLE);
    }
}
