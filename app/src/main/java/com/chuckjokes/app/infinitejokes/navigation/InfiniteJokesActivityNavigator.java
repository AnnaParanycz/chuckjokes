package com.chuckjokes.app.infinitejokes.navigation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import com.chuckjokes.app.api.navigation.ActivityNavigator;
import com.chuckjokes.app.infinitejokes.presentation.InfiniteJokesActivity;
import com.chuckjokes.domain.presentation.infinitejokes.navigation.InfiniteJokesNavigator;

import java.lang.ref.WeakReference;

public class InfiniteJokesActivityNavigator extends ActivityNavigator implements InfiniteJokesNavigator {

    private final boolean sharedTransitionSupported;
    private final String pictureTransitionName;

    private WeakReference<View> weakSharedPictureElement;

    public InfiniteJokesActivityNavigator(boolean sharedTransitionSupported,
                                          String pictureTransitionName) {

        this.sharedTransitionSupported = sharedTransitionSupported;
        this.pictureTransitionName = pictureTransitionName;

        this.weakSharedPictureElement = new WeakReference<>(null);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    protected void startActivity(Activity activity, Intent intent) {
        if (sharedTransitionSupported) {
            activity.startActivity(intent, getTransition(activity));
        } else {
            super.startActivity(activity, intent);
        }
    }

    @Override
    protected Intent createStartIntent(Activity activity) {
        return new Intent(activity, InfiniteJokesActivity.class);
    }

    private Bundle getTransition(Activity activity) {
        final View sharedPictureElement = weakSharedPictureElement.get();

        if (sharedPictureElement == null) {
            return null;
        }

        return ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                sharedPictureElement, pictureTransitionName)
                .toBundle();
    }
}
