package com.chuckjokes.app.infinitejokes.di;

import android.net.Uri;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.infinitejokes.presentation.InfiniteJokesAdapter;
import com.chuckjokes.app.jokedetails.navigation.JokeDetailsActivityNavigator;
import com.chuckjokes.domain.presentation.infinitejokes.InfiniteJokesPresenter;
import com.chuckjokes.domain.presentation.jokedetails.navigation.JokeDetailsNavigator;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.GetListOfJokesInteractor;

import dagger.Module;
import dagger.Provides;

@Module
class InfiniteJokesModule {

    @Provides
    @PerActivity
    InfiniteJokesPresenter infiniteJokesPresenter(GetListOfJokesInteractor getListOfJokesInteractor,
                                                  JokeDetailsNavigator jokeDetailsNavigator) {
        return new InfiniteJokesPresenter(getListOfJokesInteractor, jokeDetailsNavigator);
    }

    @Provides
    @PerActivity
    InfiniteJokesAdapter infiniteJokesAdapter(InfiniteJokesPresenter presenter) {
        return new InfiniteJokesAdapter(presenter);
    }

    @Provides
    @PerActivity
    JokeDetailsNavigator jokeDetailsNavigator() {
        return new JokeDetailsActivityNavigator();
    }

    @Provides
    Uri toolbarPictureLocation() {
        return Uri.parse("file:///android_asset/chuck_norris_3.jpg");
    }
}
