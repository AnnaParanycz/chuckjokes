package com.chuckjokes.app.infinitejokes.presentation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

final class ListOfJokesScrollListener extends RecyclerView.OnScrollListener {

    private final LinearLayoutManager layoutManager;

    private final Listener listener;

    public ListOfJokesScrollListener(LinearLayoutManager layoutManager,
                                     Listener listener) {
        this.layoutManager = layoutManager;
        this.listener = listener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (hasReachedBottom()) {
            listener.onJokeListBottomReached();
        }
    }

    private boolean hasReachedBottom() {
        return layoutManager.getItemCount() <= layoutManager.findLastVisibleItemPosition() + 1;
    }

    public interface Listener {
        void onJokeListBottomReached();
    }
}
