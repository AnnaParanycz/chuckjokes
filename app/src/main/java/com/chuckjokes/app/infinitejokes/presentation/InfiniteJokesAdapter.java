package com.chuckjokes.app.infinitejokes.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chuckjokes.R;
import com.chuckjokes.app.infinitejokes.presentation.model.InfiniteJokesViewTypes;
import com.chuckjokes.domain.presentation.infinitejokes.InfiniteJokesPresenter;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.ArrayList;
import java.util.List;

public class InfiniteJokesAdapter extends RecyclerView.Adapter<InfiniteJokesViewHolder> {

    private final List<Joke> jokes = new ArrayList<>();
    private final InfiniteJokesPresenter presenter;

    public InfiniteJokesAdapter(InfiniteJokesPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public InfiniteJokesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_jokes_item, parent, false);
        return new InfiniteJokesViewHolder(layoutView, viewType);
    }

    @Override
    public void onBindViewHolder(InfiniteJokesViewHolder holder, int position) {
        switch (holder.getViewType()) {
            case InfiniteJokesViewTypes.JOKE:
                if (position < jokes.size()) {
                    inflateJokeView(holder, position, jokes.get(position));
                }
                break;
            case InfiniteJokesViewTypes.LOADING:
                holder.showLoading();
                break;
            default:
                break;
        }
    }

    private void inflateJokeView(InfiniteJokesViewHolder holder, int position, Joke joke) {
        holder.setNumber(String.valueOf(position + 1));
        holder.setJoke(joke.getDisplayText());
        holder.setId(joke.getId());
        holder.setOnClickListener(presenter);
    }

    @Override
    public int getItemCount() {
        return jokes.size() + addProgressItem();
    }

    private int addProgressItem() {
        return hasJokes() ? 1 : 0;
    }

    void addJokes(List<Joke> jokes) {
        this.jokes.addAll(jokes);
        notifyDataSetChanged();
    }

    @Override
    @InfiniteJokesViewTypes.Type
    public int getItemViewType(int position) {
        return position < jokes.size() ? InfiniteJokesViewTypes.JOKE : InfiniteJokesViewTypes.LOADING;
    }

    boolean hasJokes() {
        return jokes.size() > 0;
    }
}
