package com.chuckjokes.app.infinitejokes.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chuckjokes.R;
import com.chuckjokes.app.infinitejokes.presentation.model.InfiniteJokesViewTypes;
import com.chuckjokes.domain.presentation.infinitejokes.InfiniteJokesPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

class InfiniteJokesViewHolder extends RecyclerView.ViewHolder {

    private final int viewType;
    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.jokes_list_item_text)
    TextView jokeText;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.jokes_list_item_number)
    TextView jokeNumber;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.jokes_list_item_progress)
    View loading;

    @SuppressWarnings("WeakerAccess")
    @BindView(R.id.jokes_list_item_content)
    View content;

    private String id;

    InfiniteJokesViewHolder(View itemView, int viewType) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        this.viewType = viewType;
    }

    void setJoke(String joke) {
        jokeText.setText(joke);
    }

    void setNumber(String number) {
        jokeNumber.setText(number);
    }

    void setId(String id) {
        this.id = id;
    }

    void showLoading() {
        content.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }

    @InfiniteJokesViewTypes.Type
    int getViewType() {
        return viewType;
    }

    void setOnClickListener(InfiniteJokesPresenter presenter) {
        content.setOnClickListener(v -> presenter.onJokeListItemClicked(id));
    }
}
