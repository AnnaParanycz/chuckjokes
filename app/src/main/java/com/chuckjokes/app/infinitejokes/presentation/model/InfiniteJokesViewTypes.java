package com.chuckjokes.app.infinitejokes.presentation.model;

import android.support.annotation.IntDef;

public final class InfiniteJokesViewTypes {

    private InfiniteJokesViewTypes() {
    }

    @IntDef({InfiniteJokesViewTypes.JOKE, InfiniteJokesViewTypes.LOADING})
    public @interface Type {
    }

    public static final int JOKE = 1;
    public static final int LOADING = 2;
}
