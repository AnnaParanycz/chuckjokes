package com.chuckjokes.app.jokedetails.di;

import android.app.Activity;

import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseApplication;
import com.chuckjokes.app.jokedetails.presentation.JokeDetailsActivity;

public class JokeDetailsComponentFactory implements ActivityComponentFactory<JokeDetailsComponent> {

    @Override
    public JokeDetailsComponent create(Activity activity) {
        return DaggerJokeDetailsComponent.builder()
                .jokeDetailsModule(new JokeDetailsModule())
                .applicationComponent(BaseApplication.get(activity).getApplicationComponent())
                .build();
    }

    @Override
    public void inject(JokeDetailsComponent component, Activity activity) {
        component.inject((JokeDetailsActivity) activity);
    }
}
