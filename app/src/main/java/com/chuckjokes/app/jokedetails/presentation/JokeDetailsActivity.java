package com.chuckjokes.app.jokedetails.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.chuckjokes.R;
import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseActivity;
import com.chuckjokes.app.jokedetails.di.JokeDetailsComponent;
import com.chuckjokes.app.jokedetails.di.JokeDetailsComponentFactory;
import com.chuckjokes.domain.presentation.jokedetails.JokeDetailsPresenter;
import com.chuckjokes.domain.presentation.jokedetails.model.JokeDetailsView;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class JokeDetailsActivity extends BaseActivity<JokeDetailsComponent> implements JokeDetailsView {

    public static String JOKE_DETAILS_INTENT_EXTRA_JOKE_ID = "joke_details_intent_extra_joke_id";

    @Inject
    JokeDetailsPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_joke_details_content)
    View loadedJokeContent;

    @BindView(R.id.activity_joke_details_categories)
    TextView jokeCategories;

    @BindView(R.id.activity_joke_details_loading)
    View loading;

    @BindView(R.id.activity_joke_details_error)
    TextView jokeError;

    @BindView(R.id.activity_joke_details_text)
    TextView jokeText;

    @BindView(R.id.activity_joke_details_id)
    TextView jokeId;

    @OnClick(R.id.activity_joke_details_error)
    void onJokeDetailsErrorClicked() {
        presenter.onJokeDetailsErrorClicked();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void initUi() {
        setContentView(R.layout.activity_joke_details);
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        setTitle(R.string.joke_details_title);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @NonNull
    @Override
    protected ActivityComponentFactory<JokeDetailsComponent> createActivityComponentFactory() {
        return new JokeDetailsComponentFactory();
    }

    @Override
    public String getId() {
        return getIntent().getStringExtra(JOKE_DETAILS_INTENT_EXTRA_JOKE_ID);
    }

    @Override
    public void showLoadedJoke(Joke joke) {
        jokeCategories.setText(String.format(getString(R.string.joke_details_categories_text), joke.getCategories()));
        jokeId.setText(String.format(getString(R.string.joke_details_id_text), joke.getId()));
        jokeText.setText(joke.getDisplayText());

        loading.setVisibility(GONE);
        jokeError.setVisibility(GONE);
        loadedJokeContent.setVisibility(VISIBLE);
    }

    @Override
    public void showJokeFailed() {
        loadedJokeContent.setVisibility(GONE);
        loading.setVisibility(GONE);
        jokeError.setVisibility(VISIBLE);
    }

    @Override
    public void showLoading() {
        loadedJokeContent.setVisibility(GONE);
        jokeError.setVisibility(GONE);
        loading.setVisibility(VISIBLE);
    }
}
