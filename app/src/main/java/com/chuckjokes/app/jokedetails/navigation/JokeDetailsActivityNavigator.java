package com.chuckjokes.app.jokedetails.navigation;

import android.app.Activity;
import android.content.Intent;

import com.chuckjokes.app.api.navigation.ActivityNavigator;
import com.chuckjokes.app.jokedetails.presentation.JokeDetailsActivity;
import com.chuckjokes.domain.presentation.jokedetails.navigation.JokeDetailsNavigator;

public class JokeDetailsActivityNavigator extends ActivityNavigator implements JokeDetailsNavigator {
    private String id;

    @Override
    public JokeDetailsNavigator id(String id) {
        this.id = id;
        return this;
    }

    @Override
    protected Intent createStartIntent(Activity activity) {
        final Intent intent = new Intent(activity, JokeDetailsActivity.class);
        intent.putExtra(JokeDetailsActivity.JOKE_DETAILS_INTENT_EXTRA_JOKE_ID, id);
        return intent;
    }
}
