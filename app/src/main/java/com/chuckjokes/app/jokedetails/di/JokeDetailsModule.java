package com.chuckjokes.app.jokedetails.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.domain.presentation.jokedetails.JokeDetailsPresenter;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.GetJokeInteractor;

import dagger.Module;
import dagger.Provides;

@Module
public class JokeDetailsModule {

    @Provides
    @PerActivity
    JokeDetailsPresenter jokeDetailsPresenter(GetJokeInteractor getJokeInteractor) {
        return new JokeDetailsPresenter(getJokeInteractor);
    }
}
