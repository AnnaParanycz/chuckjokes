package com.chuckjokes.app.jokedetails.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.application.di.ApplicationComponent;
import com.chuckjokes.app.jokedetails.presentation.JokeDetailsActivity;

import dagger.Component;

@PerActivity
@Component(
        modules = JokeDetailsModule.class,
        dependencies = ApplicationComponent.class
)
public interface JokeDetailsComponent {

    void inject(JokeDetailsActivity activity);
}
