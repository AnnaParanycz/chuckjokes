package com.chuckjokes.app.personalisedjoke.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.app.application.di.ApplicationComponent;
import com.chuckjokes.app.personalisedjoke.presentation.PersonalisedJokeActivity;

import dagger.Component;

@PerActivity
@Component(
        modules = PersonalisedJokeModule.class,
        dependencies = ApplicationComponent.class
)
public interface PersonalisedJokeComponent {

    void inject(PersonalisedJokeActivity activity);
}
