package com.chuckjokes.app.personalisedjoke.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chuckjokes.R;
import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseActivity;
import com.chuckjokes.app.personalisedjoke.di.PersonalisedJokeComponent;
import com.chuckjokes.app.widgets.dialog.DismissableActionDialogFragment;
import com.chuckjokes.app.personalisedjoke.di.PersonalisedJokeComponentFactory;
import com.chuckjokes.domain.api.util.StringUtil;
import com.chuckjokes.domain.presentation.personalisedjoke.PersonalisedJokePresenter;
import com.chuckjokes.domain.presentation.personalisedjoke.model.PersonalisedJokeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

import static android.view.View.GONE;

public class PersonalisedJokeActivity extends BaseActivity<PersonalisedJokeComponent> implements PersonalisedJokeView {

    @Inject
    PersonalisedJokePresenter presenter;

    @BindView(R.id.activity_personalised_joke_input_layout)
    TextInputLayout nameInputLayout;

    @BindView(R.id.activity_personalised_joke_edit_text)
    EditText nameEditText;

    @BindView(R.id.activity_personalised_joke_find_joke_button)
    Button findJokeButton;

    @BindView(R.id.activity_personalised_joke_progress)
    View loading;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.activity_personalised_joke_find_joke_button)
    void onFindJokeButtonClicked() {
        presenter.onFindJokeButtonClicked();
    }

    @OnTextChanged(
            value = R.id.activity_personalised_joke_edit_text,
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onNameUserInputChanged() {
        presenter.onNameUserInputChanged();
    }

    @OnEditorAction(value = R.id.activity_personalised_joke_edit_text)
    boolean onPersonalisedJokeAction(int actionId) {
        return presenter.onPersonalisedJokeActionGo(actionId);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void initUi() {
        setContentView(R.layout.activity_personalised_joke);
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        setTitle(R.string.joke_with_your_name);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @NonNull
    @Override
    protected ActivityComponentFactory<PersonalisedJokeComponent> createActivityComponentFactory() {
        return new PersonalisedJokeComponentFactory();
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void disableGetPersonalisedJokeButton() {
        findJokeButton.setVisibility(GONE);
    }

    @Override
    public String getName() {
        return nameEditText.getText().toString();
    }

    @Override
    public void displayPersonalisedJokeValidationError(String reason) {
        nameInputLayout.setError(reason);
        nameEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(this, android.R.drawable.ic_delete), null);
        Snackbar.make(findJokeButton, reason, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void displayPersonalisedJokeFailedDialog(String tag) {
        DismissableActionDialogFragment.newInstance(tag)
                .positiveButtonText(R.string.random_joke_failed_dialog_positive_button_text)
                .title(R.string.random_joke_failed_dialog_title)
                .message(R.string.random_joke_failed_message)
                .show(getSupportFragmentManager());
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(GONE);
    }

    @Override
    public void enableGetPersonalisedJokeButton() {
        findJokeButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayPersonalisedJokeDialog(String tag, String joke) {
        DismissableActionDialogFragment.newInstance(tag)
                .message(joke)
                .show(getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
        super.onBackPressed();
    }

    @Override
    public void hideNameValidationError() {
        nameInputLayout.setError(StringUtil.EMPTY);
        nameEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        presenter.onOptionsItemSelected(item.getItemId());
        return super.onOptionsItemSelected(item);
    }
}
