package com.chuckjokes.app.personalisedjoke.navigation;

import android.app.Activity;
import android.content.Intent;

import com.chuckjokes.app.api.navigation.ActivityNavigator;
import com.chuckjokes.app.personalisedjoke.presentation.PersonalisedJokeActivity;
import com.chuckjokes.domain.presentation.personalisedjoke.navigation.PersonalisedJokeNavigator;

public class PersonalisedJokeActivityNavigator extends ActivityNavigator implements PersonalisedJokeNavigator {

    @Override
    protected Intent createStartIntent(Activity activity) {
        return new Intent(activity, PersonalisedJokeActivity.class);
    }
}
