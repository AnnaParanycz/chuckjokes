package com.chuckjokes.app.personalisedjoke.di;

import com.chuckjokes.app.api.di.scope.PerActivity;
import com.chuckjokes.domain.presentation.personalisedjoke.PersonalisedJokePresenter;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.GetRandomPersonalisedJokeInteractor;

import dagger.Module;
import dagger.Provides;

@Module
class PersonalisedJokeModule {

    @Provides
    @PerActivity
    PersonalisedJokePresenter personalisedJokePresenter(GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor) {
        return new PersonalisedJokePresenter(getRandomPersonalisedJokeInteractor);
    }
}
