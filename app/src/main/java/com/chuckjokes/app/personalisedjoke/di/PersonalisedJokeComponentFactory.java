package com.chuckjokes.app.personalisedjoke.di;

import android.app.Activity;

import com.chuckjokes.app.api.di.ActivityComponentFactory;
import com.chuckjokes.app.application.presentation.BaseApplication;
import com.chuckjokes.app.personalisedjoke.presentation.PersonalisedJokeActivity;

public class PersonalisedJokeComponentFactory implements ActivityComponentFactory<PersonalisedJokeComponent> {

    @Override
    public PersonalisedJokeComponent create(Activity activity) {
        return DaggerPersonalisedJokeComponent.builder()
                .personalisedJokeModule(new PersonalisedJokeModule())
                .applicationComponent(BaseApplication.get(activity).getApplicationComponent())
                .build();
    }

    @Override
    public void inject(PersonalisedJokeComponent component, Activity activity) {
        component.inject((PersonalisedJokeActivity) activity);
    }
}
