package com.chuckjokes.domain.presentation.home;

import com.chuckjokes.domain.api.presentation.Presenter;
import com.chuckjokes.domain.presentation.home.model.EmptyHomeView;
import com.chuckjokes.domain.presentation.home.model.HomeView;
import com.chuckjokes.domain.presentation.infinitejokes.navigation.InfiniteJokesNavigator;
import com.chuckjokes.domain.presentation.personalisedjoke.navigation.PersonalisedJokeNavigator;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.GetRandomJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.callback.RandomJokeInteractorCallback;
import io.reactivex.disposables.Disposable;

public class HomePresenter extends Presenter<HomeView> {

    private static final String RANDOM_JOKE_DIALOG = "random_joke_dialog";
    private static final String RANDOM_JOKE_FAILED_DIALOG = "random_joke_failed_dialog";

    private final InfiniteJokesNavigator infiniteJokesNavigator;
    private final PersonalisedJokeNavigator personalisedJokeNavigator;
    private final GetRandomJokeInteractor getRandomJokeInteractor;

    private Disposable getRandomJokeDisposable;

    public HomePresenter(InfiniteJokesNavigator infiniteJokesNavigator,
                         PersonalisedJokeNavigator personalisedJokeNavigator,
                         GetRandomJokeInteractor getRandomJokeInteractor) {

        super(new EmptyHomeView());
        this.infiniteJokesNavigator = infiniteJokesNavigator;
        this.personalisedJokeNavigator = personalisedJokeNavigator;
        this.getRandomJokeInteractor = getRandomJokeInteractor;
    }

    @Override
    public void attachView(HomeView view) {
        super.attachView(view);
        view.initUi();
    }

    @Override
    public void detachView() {
        super.detachView();
        finishRunningJobs();
    }

    public void onInfiniteJokesButtonClicked() {
        finishRunningJobs();
        infiniteJokesNavigator.navigate(view);
    }

    private void finishRunningJobs() {
        if (getRandomJokeDisposable != null) {
            getRandomJokeDisposable.dispose();
            getRandomJokeDisposable = null;
            view.enableRandomJokeButton();
            view.hideLoadingRandomJoke();
        }
    }

    public void onRandomJokeButtonClicked() {
        if (getRandomJokeDisposable == null || getRandomJokeDisposable.isDisposed()) {
            view.disableRandomJokeButton();
            view.showLoadingRandomJoke();
            requestRandomJoke();
        }
    }

    private void requestRandomJoke() {
        if (getRandomJokeDisposable == null || getRandomJokeDisposable.isDisposed()) {
            getRandomJokeDisposable = getRandomJokeInteractor.execute(randomJokeInteractorCallback);
        }
    }

    private final RandomJokeInteractorCallback randomJokeInteractorCallback = new RandomJokeInteractorCallback() {
        @Override
        public void onRandomJokeReceived(Joke joke) {
            getRandomJokeDisposable = null;
            view.enableRandomJokeButton();
            view.hideLoadingRandomJoke();
            view.displayRandomJokeDialog(RANDOM_JOKE_DIALOG, joke.getDisplayText());
        }

        @Override
        public void onRandomJokeFailed() {
            getRandomJokeDisposable = null;
            view.enableRandomJokeButton();
            view.hideLoadingRandomJoke();
            view.displayRandomJokeFailedDialog(RANDOM_JOKE_FAILED_DIALOG);
        }
    };

    public void onPersonalisedJokeButtonClicked() {
        finishRunningJobs();
        personalisedJokeNavigator.navigate(view);
    }

    public void onDialogDismissed(String tag) {
        switch (tag) {
            case RANDOM_JOKE_DIALOG:
            case RANDOM_JOKE_FAILED_DIALOG:
                break;
            default:
                break;
        }
    }
}
