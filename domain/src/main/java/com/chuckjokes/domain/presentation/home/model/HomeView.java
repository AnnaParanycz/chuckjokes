package com.chuckjokes.domain.presentation.home.model;

import com.chuckjokes.domain.api.presentation.navigation.Navigatable;

public interface HomeView extends Navigatable {

    void initUi();

    void showLoadingRandomJoke();

    void disableRandomJokeButton();

    void enableRandomJokeButton();

    void hideLoadingRandomJoke();

    void displayRandomJokeDialog(String tag, String displayText);

    void displayRandomJokeFailedDialog(String tag);
}
