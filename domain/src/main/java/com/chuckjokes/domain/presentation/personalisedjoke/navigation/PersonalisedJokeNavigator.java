package com.chuckjokes.domain.presentation.personalisedjoke.navigation;

import com.chuckjokes.domain.api.presentation.navigation.Navigator;

public interface PersonalisedJokeNavigator extends Navigator {
}
