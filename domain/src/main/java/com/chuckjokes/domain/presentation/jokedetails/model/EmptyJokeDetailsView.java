package com.chuckjokes.domain.presentation.jokedetails.model;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

public class EmptyJokeDetailsView implements JokeDetailsView {
    @Override
    public void initUi() {

    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void showLoadedJoke(Joke joke) {

    }

    @Override
    public void showJokeFailed() {

    }

    @Override
    public void showLoading() {

    }
}
