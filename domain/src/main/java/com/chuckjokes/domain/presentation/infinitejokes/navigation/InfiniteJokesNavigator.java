package com.chuckjokes.domain.presentation.infinitejokes.navigation;

import com.chuckjokes.domain.api.presentation.navigation.Navigator;

public interface InfiniteJokesNavigator extends Navigator {
}
