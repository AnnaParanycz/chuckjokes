package com.chuckjokes.domain.presentation.personalisedjoke;

import com.chuckjokes.domain.api.presentation.Presenter;
import com.chuckjokes.domain.api.presentation.usercommands.KeyboardAction;
import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.presentation.personalisedjoke.model.PersonalisedJokeView;
import com.chuckjokes.domain.presentation.personalisedjoke.model.EmptyPersonalisedJokeView;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.GetRandomPersonalisedJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.callback.RandomPersonalisedJokeInteractorCallback;

import io.reactivex.disposables.Disposable;

public class PersonalisedJokePresenter extends Presenter<PersonalisedJokeView> {

    private static final String PERSONALISED_JOKE_DIALOG = "personalised_joke_dialog";
    private static final String JOKE_FAILED_DIALOG = "joke_failed_dialog";

    private final GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor;

    private Disposable personalisedJokeDisposable;
    private final RandomPersonalisedJokeInteractorCallback randomPersonalisedJokeInteractorCallback = new RandomPersonalisedJokeInteractorCallback() {
        @Override
        public void onRandomPersonalisedJokeFailed() {
            personalisedJokeDisposable = null;
            view.displayPersonalisedJokeFailedDialog(JOKE_FAILED_DIALOG);
            enableView();
        }

        @Override
        public void onRandomPersonalisedJokeInvalid(String reason) {
            personalisedJokeDisposable = null;
            view.displayPersonalisedJokeValidationError(reason);
            enableView();
        }

        @Override
        public void onRandomPersonalisedJokeJokeReceived(String joke) {
            personalisedJokeDisposable = null;
            view.displayPersonalisedJokeDialog(PERSONALISED_JOKE_DIALOG, joke);
            enableView();
        }
    };

    public PersonalisedJokePresenter(GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor) {
        super(new EmptyPersonalisedJokeView());

        this.getRandomPersonalisedJokeInteractor = getRandomPersonalisedJokeInteractor;
    }

    @Override
    public void attachView(PersonalisedJokeView view) {
        super.attachView(view);
        view.initUi();
    }

    @Override
    public void detachView() {
        super.detachView();
        finishRunningJobs();
    }

    private void finishRunningJobs() {
        if (personalisedJokeDisposable != null) {
            personalisedJokeDisposable.dispose();
            personalisedJokeDisposable = null;
        }
    }

    public void onFindJokeButtonClicked() {
        requestPersonalisedJoke();
    }

    private void requestPersonalisedJoke() {
        if (personalisedJokeDisposable == null || personalisedJokeDisposable.isDisposed()) {
            disableView();
            personalisedJokeDisposable = getRandomPersonalisedJokeInteractor.execute(view.getName(), randomPersonalisedJokeInteractorCallback);
        }
    }

    private void disableView() {
        view.showLoading();
        view.disableGetPersonalisedJokeButton();
    }

    private void enableView() {
        view.hideLoading();
        view.enableGetPersonalisedJokeButton();
    }

    public void onBackPressed() {
        finishRunningJobs();
    }

    public void onNameUserInputChanged() {
        view.hideNameValidationError();
    }

    @SuppressWarnings("SameReturnValue")
    public boolean onPersonalisedJokeActionGo(int action) {
        if (action == KeyboardAction.actionGo()) {
            requestPersonalisedJoke();
        }
        return false;
    }

    public void onOptionsItemSelected(int itemId) {
        if (itemId == NavigationAction.up()) {
            finishRunningJobs();
        }
    }
}
