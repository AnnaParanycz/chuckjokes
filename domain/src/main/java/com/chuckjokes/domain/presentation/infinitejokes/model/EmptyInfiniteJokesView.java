package com.chuckjokes.domain.presentation.infinitejokes.model;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

public class EmptyInfiniteJokesView implements InfiniteJokesView {
    @Override
    public void initUi() {

    }

    @Override
    public void addJokes(List<Joke> strings) {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showLoadingScreen() {

    }

    @Override
    public void showLoaded() {

    }

    @Override
    public boolean hasJokes() {
        return false;
    }

    @Override
    public Object getNavigatable() {
        return null;
    }

    @Override
    public void showNoJokesAvailableError() {

    }
}
