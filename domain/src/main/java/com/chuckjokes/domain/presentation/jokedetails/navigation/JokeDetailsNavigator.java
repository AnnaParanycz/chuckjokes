package com.chuckjokes.domain.presentation.jokedetails.navigation;

import com.chuckjokes.domain.api.presentation.navigation.Navigator;

public interface JokeDetailsNavigator extends Navigator {
    JokeDetailsNavigator id(String id);
}
