package com.chuckjokes.domain.presentation.personalisedjoke.model;

public class EmptyPersonalisedJokeView implements PersonalisedJokeView {
    @Override
    public void initUi() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void disableGetPersonalisedJokeButton() {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void displayPersonalisedJokeValidationError(String reason) {

    }

    @Override
    public void displayPersonalisedJokeFailedDialog(String tag) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void enableGetPersonalisedJokeButton() {

    }

    @Override
    public void displayPersonalisedJokeDialog(String tag, String joke) {

    }

    @Override
    public void hideNameValidationError() {

    }
}
