package com.chuckjokes.domain.presentation.jokedetails;

import com.chuckjokes.domain.api.presentation.Presenter;
import com.chuckjokes.domain.presentation.jokedetails.model.EmptyJokeDetailsView;
import com.chuckjokes.domain.presentation.jokedetails.model.JokeDetailsView;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.GetJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.callback.GetJokeInteractorCallback;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.model.GetJokeInteractorRequest;

import io.reactivex.disposables.Disposable;

public class JokeDetailsPresenter extends Presenter<JokeDetailsView> {

    private final GetJokeInteractor getJokeInteractor;

    private Disposable getJokeDisposable;

    public JokeDetailsPresenter(GetJokeInteractor getJokeInteractor) {
        super(new EmptyJokeDetailsView());
        this.getJokeInteractor = getJokeInteractor;
    }

    @Override
    public void attachView(JokeDetailsView view) {
        super.attachView(view);
        view.initUi();

        requestJokeDetails();
    }

    @Override
    public void detachView() {
        super.detachView();
        if (getJokeDisposable != null) {
            getJokeDisposable.dispose();
            getJokeDisposable = null;
        }
    }

    private void requestJokeDetails() {
        view.showLoading();

        getJokeDisposable = getJokeInteractor.execute(
                new GetJokeInteractorRequest(view.getId()),
                new GetJokeInteractorCallback() {
                    @Override
                    public void onJokeReceived(Joke joke) {
                        view.showLoadedJoke(joke);
                        getJokeDisposable = null;
                    }

                    @Override
                    public void onJokeFailed() {
                        view.showJokeFailed();
                        getJokeDisposable = null;
                    }
                });
    }

    public void onJokeDetailsErrorClicked() {
        requestJokeDetails();
    }
}
