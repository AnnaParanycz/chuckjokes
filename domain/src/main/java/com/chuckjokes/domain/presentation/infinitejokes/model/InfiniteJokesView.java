package com.chuckjokes.domain.presentation.infinitejokes.model;

import com.chuckjokes.domain.api.presentation.navigation.Navigatable;
import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

public interface InfiniteJokesView extends Navigatable {
    void initUi();

    void addJokes(List<Joke> strings);

    void showError();

    void showLoadingScreen();

    void showLoaded();

    boolean hasJokes();

    void showNoJokesAvailableError();
}
