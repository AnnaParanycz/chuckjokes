package com.chuckjokes.domain.presentation.jokedetails.model;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

public interface JokeDetailsView {
    void initUi();

    String getId();

    void showLoadedJoke(Joke joke);

    void showJokeFailed();

    void showLoading();
}
