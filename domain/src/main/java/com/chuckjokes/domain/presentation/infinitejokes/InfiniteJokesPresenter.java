package com.chuckjokes.domain.presentation.infinitejokes;

import com.chuckjokes.domain.api.presentation.Presenter;
import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.presentation.infinitejokes.model.EmptyInfiniteJokesView;
import com.chuckjokes.domain.presentation.infinitejokes.model.InfiniteJokesView;
import com.chuckjokes.domain.presentation.jokedetails.navigation.JokeDetailsNavigator;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.GetListOfJokesInteractor;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.callback.ListOfJokesInteractorCallback;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class InfiniteJokesPresenter extends Presenter<InfiniteJokesView> {

    private static final int JOKES_BATCH_COUNT = 20;

    private final GetListOfJokesInteractor getListOfJokesInteractor;
    private final JokeDetailsNavigator jokeDetailsNavigator;

    private Disposable jokesDisposable;

    private final ListOfJokesInteractorCallback listOfJokesInteractorCallback = new ListOfJokesInteractorCallback() {
        @Override
        public void onJokesReceived(List<Joke> jokes) {
            jokesDisposable = null;
            view.addJokes(jokes);
            view.showLoaded();
        }

        @Override
        public void onJokesFailed() {
            jokesDisposable = null;
            view.showError();
        }

        @Override
        public void onNoJokesAvailable() {
            jokesDisposable = null;
            view.showNoJokesAvailableError();
        }
    };

    public InfiniteJokesPresenter(GetListOfJokesInteractor getListOfJokesInteractor,
                                  JokeDetailsNavigator jokeDetailsNavigator) {

        super(new EmptyInfiniteJokesView());
        this.getListOfJokesInteractor = getListOfJokesInteractor;
        this.jokeDetailsNavigator = jokeDetailsNavigator;
    }

    @Override
    public void attachView(InfiniteJokesView view) {
        super.attachView(view);
        view.initUi();

        if (view.hasJokes()) {
            view.showLoaded();
        } else {
            view.showLoadingScreen();
            requestMoreJokes();
        }
    }

    public void onJokeListBottomReached() {
        requestMoreJokes();
    }

    public void onRetryToLoadJokes() {
        requestMoreJokes();
    }

    void requestMoreJokes() {
        if (jokesDisposable == null || jokesDisposable.isDisposed()) {
            jokesDisposable = getListOfJokesInteractor.execute(JOKES_BATCH_COUNT, listOfJokesInteractorCallback);
        }
    }

    public void onOptionsItemSelected(int itemId) {
        if (itemId == NavigationAction.up()) {
            finishRunningJobs();
        }
    }

    public void onBackPressed() {
        finishRunningJobs();
    }

    private void finishRunningJobs() {
        if (jokesDisposable != null) {
            jokesDisposable.dispose();
            jokesDisposable = null;
        }
    }

    public void onJokeListItemClicked(String id) {
        jokeDetailsNavigator.id(id).navigate(view);
    }
}
