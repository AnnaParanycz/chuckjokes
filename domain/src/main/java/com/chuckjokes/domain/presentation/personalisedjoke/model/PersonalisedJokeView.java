package com.chuckjokes.domain.presentation.personalisedjoke.model;

public interface PersonalisedJokeView {
    void initUi();

    void showLoading();

    void disableGetPersonalisedJokeButton();

    String getName();

    void displayPersonalisedJokeValidationError(String reason);

    void displayPersonalisedJokeFailedDialog(String tag);

    void hideLoading();

    void enableGetPersonalisedJokeButton();

    void displayPersonalisedJokeDialog(String tag, String joke);

    void hideNameValidationError();
}
