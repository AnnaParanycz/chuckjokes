package com.chuckjokes.domain.presentation.home.model;

public class EmptyHomeView implements HomeView {

    @Override
    public void initUi() {

    }

    @Override
    public void showLoadingRandomJoke() {

    }

    @Override
    public void disableRandomJokeButton() {

    }

    @Override
    public void enableRandomJokeButton() {

    }

    @Override
    public void hideLoadingRandomJoke() {

    }

    @Override
    public void displayRandomJokeDialog(String tag, String displayText) {

    }

    @Override
    public void displayRandomJokeFailedDialog(String tag) {

    }

    @Override
    public Object getNavigatable() {
        return null;
    }
}
