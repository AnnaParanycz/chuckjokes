package com.chuckjokes.domain.api.presentation.navigation;

public interface Navigatable {

    Object getNavigatable();
}
