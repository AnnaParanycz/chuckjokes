package com.chuckjokes.domain.api.presentation.navigation;

public interface Navigator {

    void navigate(Navigatable navigatable);
}
