package com.chuckjokes.domain.api.presentation.usercommands;

@SuppressWarnings("SameReturnValue")
public abstract class KeyboardAction {

    private static KeyboardAction keyboardAction;

    public static void setInstance(KeyboardAction keyboardAction) {
        KeyboardAction.keyboardAction = keyboardAction;
    }

    public static int actionGo() {
        return keyboardAction.getActionGo();
    }

    protected abstract int getActionGo();
}
