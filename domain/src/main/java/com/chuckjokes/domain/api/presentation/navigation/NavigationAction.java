package com.chuckjokes.domain.api.presentation.navigation;

@SuppressWarnings("SameReturnValue")
public abstract class NavigationAction {

    private static NavigationAction navigationAction;

    public static void setInstance(NavigationAction navigationAction) {
        NavigationAction.navigationAction = navigationAction;
    }

    public static int up() {
        return navigationAction.getUp();
    }

    public abstract int getUp();
}
