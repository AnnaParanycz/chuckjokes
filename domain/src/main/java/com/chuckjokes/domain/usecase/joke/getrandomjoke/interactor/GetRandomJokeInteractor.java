package com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.callback.RandomJokeInteractorCallback;

import io.reactivex.disposables.Disposable;

public class GetRandomJokeInteractor {

    private final JokeRepository jokeRepository;
    private final SchedulerProvider schedulerProvider;

    public GetRandomJokeInteractor(JokeRepository jokeRepository,
                                   SchedulerProvider schedulerProvider) {
        this.jokeRepository = jokeRepository;
        this.schedulerProvider = schedulerProvider;
    }

    public Disposable execute(RandomJokeInteractorCallback randomJokeInteractorCallback) {
        return jokeRepository.getRandomJoke()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(randomJokeInteractorCallback::onRandomJokeReceived,
                        throwable -> randomJokeInteractorCallback.onRandomJokeFailed());
    }
}
