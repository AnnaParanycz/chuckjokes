package com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.callback;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

public interface RandomJokeInteractorCallback {

    void onRandomJokeReceived(Joke joke);

    void onRandomJokeFailed();
}
