package com.chuckjokes.domain.usecase.joke.entity;

public class Joke {

    private final String displayText;
    private final String categories;
    private final String id;

    public Joke(String displayText, String id, String categories) {
        this.displayText = displayText;
        this.categories = categories;
        this.id = id;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getCategories() {
        return categories;
    }

    public String getId() {
        return id;
    }
}
