package com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.callback;

public interface RandomPersonalisedJokeInteractorCallback {
    void onRandomPersonalisedJokeFailed();

    void onRandomPersonalisedJokeInvalid(String reason);

    void onRandomPersonalisedJokeJokeReceived(String jokeDisplayText);
}
