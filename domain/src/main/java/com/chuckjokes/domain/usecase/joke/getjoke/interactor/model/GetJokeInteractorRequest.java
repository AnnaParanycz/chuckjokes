package com.chuckjokes.domain.usecase.joke.getjoke.interactor.model;

public class GetJokeInteractorRequest {

    private final String id;

    public GetJokeInteractorRequest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
