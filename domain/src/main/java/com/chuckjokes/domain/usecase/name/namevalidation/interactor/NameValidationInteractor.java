package com.chuckjokes.domain.usecase.name.namevalidation.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.callback.NameValidationInteractorCallback;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.model.NameValidationRequest;

import io.reactivex.disposables.Disposable;

@SuppressWarnings({"SameParameterValue", "WeakerAccess"})
public class NameValidationInteractor {

    private final NameValidationRepository nameValidationRepository;
    private final SchedulerProvider schedulerProvider;

    public NameValidationInteractor(SchedulerProvider schedulerProvider,
                                    NameValidationRepository nameValidationRepository) {

        this.schedulerProvider = schedulerProvider;
        this.nameValidationRepository = nameValidationRepository;
    }

    public Disposable execute(String userInput, NameValidationInteractorCallback callback) {
        return nameValidationRepository.getValidatedName(new NameValidationRequest(userInput))
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(name -> callback.onNameInputValid(), throwable -> callback.onNameInputInvalid());
    }
}
