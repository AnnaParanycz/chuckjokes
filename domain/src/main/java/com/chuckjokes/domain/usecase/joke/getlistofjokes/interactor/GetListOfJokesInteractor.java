package com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.NoJokesAvailableException;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.callback.ListOfJokesInteractorCallback;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class GetListOfJokesInteractor {

    private final SchedulerProvider schedulerProvider;
    private final JokeRepository jokeRepository;

    public GetListOfJokesInteractor(SchedulerProvider schedulerProvider,
                                    JokeRepository jokeRepository) {

        this.schedulerProvider = schedulerProvider;
        this.jokeRepository = jokeRepository;
    }

    public Disposable execute(int count, ListOfJokesInteractorCallback callback) {
        if (count <= 0) {
            return null;
        }

        return jokeRepository.getListOfJokes(new ListOfJokesRequest(count))
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        callback::onJokesReceived,
                        getThrowableConsumer(callback)
                );
    }

    private Consumer<Throwable> getThrowableConsumer(ListOfJokesInteractorCallback callback) {
        return throwable -> {
            if (throwable instanceof NoJokesAvailableException) {
                callback.onNoJokesAvailable();
            } else {
                callback.onJokesFailed();
            }
        };
    }
}
