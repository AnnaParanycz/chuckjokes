package com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model;

public class ListOfJokesRequest {

    private final int count;

    public ListOfJokesRequest(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
