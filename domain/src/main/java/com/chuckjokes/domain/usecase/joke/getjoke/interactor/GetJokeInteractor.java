package com.chuckjokes.domain.usecase.joke.getjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.api.util.StringUtil;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.callback.GetJokeInteractorCallback;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.model.GetJokeInteractorRequest;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

public class GetJokeInteractor {

    private final JokeRepository jokeRepository;
    private final SchedulerProvider schedulerProvider;

    public GetJokeInteractor(JokeRepository jokeRepository,
                             SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
        this.jokeRepository = jokeRepository;
    }

    public Disposable execute(GetJokeInteractorRequest request, GetJokeInteractorCallback callback) {
        return validateRequest(request)
                .flatMap(validatedRequest -> jokeRepository.getJoke(validatedRequest.getId()))
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(callback::onJokeReceived,
                        throwable -> callback.onJokeFailed());
    }

    private Single<GetJokeInteractorRequest> validateRequest(GetJokeInteractorRequest request) {
        if (request == null || StringUtil.isBlank(request.getId())) {
            return Single.error(new RuntimeException("Invalid id"));
        } else {
            return Single.just(request);
        }
    }
}
