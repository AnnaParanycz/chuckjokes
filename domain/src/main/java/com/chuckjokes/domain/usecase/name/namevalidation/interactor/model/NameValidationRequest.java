package com.chuckjokes.domain.usecase.name.namevalidation.interactor.model;

public class NameValidationRequest {

    private final String userInput;

    public NameValidationRequest(String userInput) {
        this.userInput = userInput;
    }

    public String getUserInput() {
        return userInput;
    }
}
