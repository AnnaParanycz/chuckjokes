package com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.callback;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

import java.util.List;

public interface ListOfJokesInteractorCallback {
    void onJokesReceived(List<Joke> jokes);

    void onJokesFailed();

    void onNoJokesAvailable();
}
