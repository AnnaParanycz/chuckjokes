package com.chuckjokes.domain.usecase.name.namevalidation.repository;

import com.chuckjokes.domain.usecase.name.namevalidation.interactor.model.NameValidationRequest;
import com.chuckjokes.domain.usecase.name.entity.Name;

import io.reactivex.Single;

public interface NameValidationRepository {

    Single<Name> getValidatedName(NameValidationRequest request);
}
