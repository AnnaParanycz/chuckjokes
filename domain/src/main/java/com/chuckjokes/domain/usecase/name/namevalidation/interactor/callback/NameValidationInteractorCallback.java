package com.chuckjokes.domain.usecase.name.namevalidation.interactor.callback;

public interface NameValidationInteractorCallback {
    void onNameInputValid();

    void onNameInputInvalid();
}
