package com.chuckjokes.domain.usecase.name.entity;

public class InvalidNameException extends RuntimeException {

    public InvalidNameException(String message) {
        super(message);
    }
}
