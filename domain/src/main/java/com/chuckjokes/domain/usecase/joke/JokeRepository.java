package com.chuckjokes.domain.usecase.joke;

import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.model.ListOfJokesRequest;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;

import java.util.List;

import io.reactivex.Single;

public interface JokeRepository {

    Single<List<Joke>> getListOfJokes(ListOfJokesRequest request);

    Single<Joke> getJoke(String id);

    Single<Joke> getRandomJoke();

    Single<Joke> getRandomPersonalisedJoke(RandomPersonalisedJokeRequest request);
}
