package com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model;

import com.chuckjokes.domain.usecase.name.entity.Name;

public class RandomPersonalisedJokeRequest {

    private final Name name;

    public RandomPersonalisedJokeRequest(Name name) {
        this.name = name;
    }

    public Name getName() {
        return name;
    }
}
