package com.chuckjokes.domain.usecase.joke.getjoke.interactor.callback;

import com.chuckjokes.domain.usecase.joke.entity.Joke;

public interface GetJokeInteractorCallback {

    void onJokeReceived(Joke joke);

    void onJokeFailed();
}
