package com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.callback.RandomPersonalisedJokeInteractorCallback;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.model.RandomPersonalisedJokeRequest;
import com.chuckjokes.domain.usecase.name.entity.InvalidNameException;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.model.NameValidationRequest;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;

import io.reactivex.disposables.Disposable;

public class GetRandomPersonalisedJokeInteractor {

    private final NameValidationRepository nameValidationRepository;
    private final SchedulerProvider schedulerProvider;
    private final JokeRepository jokeRepository;

    public GetRandomPersonalisedJokeInteractor(JokeRepository jokeRepository,
                                               SchedulerProvider schedulerProvider,
                                               NameValidationRepository nameValidationRepository) {

        this.nameValidationRepository = nameValidationRepository;
        this.schedulerProvider = schedulerProvider;
        this.jokeRepository = jokeRepository;
    }

    public Disposable execute(String userInput, RandomPersonalisedJokeInteractorCallback callback) {
        return nameValidationRepository.getValidatedName(new NameValidationRequest(userInput))
                .flatMap(name -> jokeRepository.getRandomPersonalisedJoke(new RandomPersonalisedJokeRequest(name)))
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        joke -> callback.onRandomPersonalisedJokeJokeReceived(joke.getDisplayText()),
                        throwable -> resolveExceptions(callback, throwable));
    }

    private void resolveExceptions(RandomPersonalisedJokeInteractorCallback callback, Throwable throwable) {
        try {
            throw throwable;
        } catch (InvalidNameException invalidNameException) {
            callback.onRandomPersonalisedJokeInvalid(invalidNameException.getMessage());
        } catch (Throwable allOtherExceptions) {
            callback.onRandomPersonalisedJokeFailed();
        }
    }
}
