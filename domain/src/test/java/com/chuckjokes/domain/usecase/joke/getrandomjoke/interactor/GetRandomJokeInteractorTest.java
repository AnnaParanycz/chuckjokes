package com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.callback.RandomJokeInteractorCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetRandomJokeInteractorTest {

    private final Joke joke = new Joke("Joke_id", "Joke_text", "");

    private GetRandomJokeInteractor getRandomJokeInteractor;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private JokeRepository jokeRepository;

    @Mock
    private RandomJokeInteractorCallback randomJokeInteractorCallback;

    @Mock
    private SchedulerProvider schedulerProvider;

    private TestScheduler testScheduler;

    @Before
    public void setup() {
        testScheduler = new TestScheduler();
        when(schedulerProvider.io()).thenReturn(testScheduler);
        when(schedulerProvider.ui()).thenReturn(testScheduler);
        getRandomJokeInteractor = new GetRandomJokeInteractor(jokeRepository, schedulerProvider);
    }

    @Test
    public void should_request_joke_from_repository() {
        // given
        givenRandomJokeRepositoryCallWillReturnSuccessfulJoke();

        // when
        getRandomJokeInteractor.execute(randomJokeInteractorCallback);

        // then
        verify(jokeRepository, only()).getRandomJoke();
    }

    @Test
    public void should_return_joke_to_callback_when_random_joke_call_was_successful() {
        givenRandomJokeRepositoryCallWillReturnSuccessfulJoke();

        // when
        getRandomJokeInteractor.execute(randomJokeInteractorCallback);
        testScheduler.triggerActions();

        // then
        verify(randomJokeInteractorCallback, only()).onRandomJokeReceived(joke);
    }

    @Test
    public void should_return_failure_to_callback_when_random_joke_call_failed() {
        givenRandomJokeRepositoryCallWillReturnFailure();

        // when
        getRandomJokeInteractor.execute(randomJokeInteractorCallback);
        testScheduler.triggerActions();

        // then
        verify(randomJokeInteractorCallback, only()).onRandomJokeFailed();
    }

    private void givenRandomJokeRepositoryCallWillReturnSuccessfulJoke() {
        when(jokeRepository.getRandomJoke()).thenReturn(Single.just(joke));
    }

    private void givenRandomJokeRepositoryCallWillReturnFailure() {
        when(jokeRepository.getRandomJoke()).thenReturn(Single.error(new RuntimeException()));
    }
}
