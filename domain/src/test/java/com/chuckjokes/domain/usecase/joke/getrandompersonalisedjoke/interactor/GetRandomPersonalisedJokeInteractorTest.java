package com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.callback.RandomPersonalisedJokeInteractorCallback;
import com.chuckjokes.domain.usecase.name.entity.InvalidNameException;
import com.chuckjokes.domain.usecase.name.entity.Name;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static com.chuckjokes.domain.api.testutil.CustomAssertions.argThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetRandomPersonalisedJokeInteractorTest {

    private final Name name = new Name("first", "last");
    private final String userInput = "some name";
    private final TestScheduler testScheduler = new TestScheduler();
    private GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor;
    @Mock
    private JokeRepository jokeRepository;
    @Mock
    private RandomPersonalisedJokeInteractorCallback callback;
    @Mock
    private SchedulerProvider schedulerProvider;
    @Mock
    private NameValidationRepository nameValidationRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(schedulerProvider.io()).thenReturn(testScheduler);
        when(schedulerProvider.ui()).thenReturn(testScheduler);
        getRandomPersonalisedJokeInteractor = new GetRandomPersonalisedJokeInteractor(jokeRepository, schedulerProvider, nameValidationRepository);
    }

    @Test
    public void should_request_personalised_joke_from_repository_if_name_is_received_from_name_gateway() {
        // given
        givenNameGatewayWillReturnName();
        givenRepositoryWillReturnJoke();

        // when
        getRandomPersonalisedJokeInteractor.execute(userInput, callback);
        testScheduler.triggerActions();

        // then
        verify(jokeRepository).getRandomPersonalisedJoke(argThat(request -> {
            assertThat(request).isNotNull();
            assertThat(request.getName()).isEqualTo(name);
        }));
    }

    private void givenNameGatewayWillReturnName() {
        when(nameValidationRepository.getValidatedName(any())).thenReturn(Single.just(name));
    }

    private Joke givenRepositoryWillReturnJoke() {
        final Joke joke = new Joke("23", "joke text", "Cat1, Cat2");
        when(jokeRepository.getRandomPersonalisedJoke(any())).thenReturn(Single.just(joke));
        return joke;
    }

    @Test
    public void should_return_joke_to_callback_when_joke_received() {
        givenNameGatewayWillReturnName();
        final Joke joke = givenRepositoryWillReturnJoke();

        // when
        getRandomPersonalisedJokeInteractor.execute(userInput, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onRandomPersonalisedJokeJokeReceived(joke.getDisplayText());
    }

    @Test
    public void should_return_failure_if_repository_fails() {
        givenNameGatewayWillReturnName();
        givenRepositoryWillFail();

        // when
        getRandomPersonalisedJokeInteractor.execute(userInput, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onRandomPersonalisedJokeFailed();
    }

    private void givenRepositoryWillFail() {
        when(jokeRepository.getRandomPersonalisedJoke(any())).thenReturn(Single.error(new RuntimeException()));
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Test
    public void should_return_validation_failure_if_name_not_received() {
        // given
        InvalidNameException exception = givenNameGatewayWillFail();

        // when
        getRandomPersonalisedJokeInteractor.execute(userInput, callback);
        testScheduler.triggerActions();

        // then
        verify(nameValidationRepository).getValidatedName(any());
        verify(callback, only()).onRandomPersonalisedJokeInvalid(exception.getMessage());
        verifyZeroInteractions(jokeRepository);
    }

    private InvalidNameException givenNameGatewayWillFail() {
        final InvalidNameException invalidNameException = new InvalidNameException("invalid message");
        when(nameValidationRepository.getValidatedName(any())).thenReturn(Single.error(invalidNameException));
        return invalidNameException;
    }
}
