package com.chuckjokes.domain.usecase.joke.getjoke.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.callback.GetJokeInteractorCallback;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.model.GetJokeInteractorRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetJokeInteractorTest {

    private final Joke joke = new Joke("Joke_id", "Joke_text", "Cat1, Cat2");

    private GetJokeInteractor getJokeInteractor;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private JokeRepository jokeRepository;

    @Mock
    private GetJokeInteractorCallback callback;

    @Mock
    private SchedulerProvider schedulerProvider;

    @Mock
    private GetJokeInteractorRequest request;

    private TestScheduler testScheduler;

    @Before
    public void setup() {
        testScheduler = new TestScheduler();
        when(schedulerProvider.io()).thenReturn(testScheduler);
        when(schedulerProvider.ui()).thenReturn(testScheduler);
        getJokeInteractor = new GetJokeInteractor(jokeRepository, schedulerProvider);
    }

    @Test
    public void should_request_joke_from_repository() {
        // given
        givenJokeRepositoryCallWillReturnSuccessfulJoke();
        String id = givenJokeRequestHasJokeId();

        // when
        getJokeInteractor.execute(request, callback);
        testScheduler.triggerActions();

        // then
        verify(request, atLeastOnce()).getId();
        verify(jokeRepository, only()).getJoke(id);
    }

    @Test
    public void should_return_joke_to_callback_when_random_joke_call_was_successful() {
        givenJokeRepositoryCallWillReturnSuccessfulJoke();
        givenJokeRequestHasJokeId();

        // when
        getJokeInteractor.execute(request, callback);
        testScheduler.triggerActions();

        // then
        verify(request, atLeastOnce()).getId();
        verify(callback, only()).onJokeReceived(joke);
    }

    @Test
    public void should_return_failure_to_callback_when_random_joke_call_failed() {
        // when
        givenJokeRepositoryCallWillReturnFailure();
        givenJokeRequestHasJokeId();

        // when
        getJokeInteractor.execute(request, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onJokeFailed();
    }

    @Test
    public void should_return_failure_to_callback_when_request_is_missing() {
        // when
        getJokeInteractor.execute(null, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onJokeFailed();
    }

    @Test
    public void should_return_failure_to_callback_when_joke_id_is_missing() {
        // when
        getJokeInteractor.execute(request, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onJokeFailed();
    }

    private String givenJokeRequestHasJokeId() {
        String id = "joke_id";
        when(request.getId()).thenReturn(id);
        return id;
    }

    private void givenJokeRepositoryCallWillReturnSuccessfulJoke() {
        when(jokeRepository.getJoke(any())).thenReturn(Single.just(joke));
    }

    private void givenJokeRepositoryCallWillReturnFailure() {
        when(jokeRepository.getJoke(any())).thenReturn(Single.error(new RuntimeException()));
    }
}
