package com.chuckjokes.domain.usecase.name.namevalidation.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.usecase.name.namevalidation.repository.NameValidationRepository;
import com.chuckjokes.domain.usecase.name.namevalidation.interactor.callback.NameValidationInteractorCallback;
import com.chuckjokes.domain.usecase.name.entity.Name;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static com.chuckjokes.domain.api.testutil.CustomAssertions.argThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NameValidationInteractorTest {

    private static final String SOME_NAME = "some_name";
    private final TestScheduler testScheduler = new TestScheduler();
    private NameValidationInteractor nameValidationInteractor;
    @Mock
    private SchedulerProvider schedulerProvider;
    @Mock
    private NameValidationInteractorCallback callback;
    @Mock
    private NameValidationRepository nameValidationRepository;

    @Before
    public void setup() {
        when(schedulerProvider.ui()).thenReturn(testScheduler);
        when(schedulerProvider.io()).thenReturn(testScheduler);
        nameValidationInteractor = new NameValidationInteractor(schedulerProvider, nameValidationRepository);
    }

    @Test
    public void should_request_name_gateway_upon_user_input() {
        // given
        givenThatNameGatewayWillReturnName();

        // when
        nameValidationInteractor.execute(SOME_NAME, callback);
        testScheduler.triggerActions();

        // then
        verify(nameValidationRepository).getValidatedName(argThat(
                request -> Assertions.assertThat(request.getUserInput()).isEqualTo(SOME_NAME)));
        verify(schedulerProvider).io();
        verify(schedulerProvider).ui();
    }

    private void givenThatNameGatewayWillReturnName() {
        when(nameValidationRepository.getValidatedName(any())).thenReturn(Single.just(new Name("first", "last")));
    }

    @Test
    public void should_return_successful_validation_when_name_is_valid() {
        // given
        givenThatNameGatewayWillReturnName();

        // when
        nameValidationInteractor.execute(SOME_NAME, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onNameInputValid();
    }

    @Test
    public void should_return_failed_validation_when_name_is_invalid() {
        // given
        givenThatNameGatewayWillReturnFailure();

        // when
        nameValidationInteractor.execute(SOME_NAME, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onNameInputInvalid();
    }

    private void givenThatNameGatewayWillReturnFailure() {
        when(nameValidationRepository.getValidatedName(any())).thenReturn(Single.error(new RuntimeException()));
    }
}
