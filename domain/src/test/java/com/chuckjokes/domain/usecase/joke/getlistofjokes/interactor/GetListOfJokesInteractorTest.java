package com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor;

import com.chuckjokes.domain.api.java.rx.SchedulerProvider;
import com.chuckjokes.domain.api.testutil.CustomAssertions;
import com.chuckjokes.domain.usecase.joke.JokeRepository;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.callback.ListOfJokesInteractorCallback;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.TestScheduler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetListOfJokesInteractorTest {

    private static final int COUNT = 30;

    private GetListOfJokesInteractor getListOfJokesInteractor;

    private List<Joke> jokeResponse = Arrays.asList(
            new Joke("Joke 1", "Id 1", ""),
            new Joke("Joke 2", "Id 2", ""),
            new Joke("Joke 3", "Id 3", "")
    );

    @Mock
    private SchedulerProvider schedulerProvider;

    @Mock
    private JokeRepository jokeRepository;

    @Mock
    private ListOfJokesInteractorCallback callback;

    private TestScheduler testScheduler = new TestScheduler();

    @Before
    public void setup() {
        getListOfJokesInteractor = new GetListOfJokesInteractor(schedulerProvider, jokeRepository);
        when(schedulerProvider.ui()).thenReturn(testScheduler);
        when(schedulerProvider.io()).thenReturn(testScheduler);
    }

    @Test
    public void should_do_nothing_when_count_is_zero() {
        // when
        final Disposable disposable = getListOfJokesInteractor.execute(0, callback);

        // then
        assertThat(disposable).isNull();
        verifyZeroInteractions(jokeRepository);
    }

    @Test
    public void request_jokes_when_requesting_valid_count() {
        // given
        givenRepositoryWillReturnJokes();

        // when
        getListOfJokesInteractor.execute(COUNT, callback);

        // then
        verify(jokeRepository).getListOfJokes(CustomAssertions.argThat(request -> Assertions.assertThat(request.getCount()).isEqualTo(COUNT)));
    }

    @Test
    public void should_return_mapped_jokes() {
        // given
        givenRepositoryWillReturnJokes();

        // when
        getListOfJokesInteractor.execute(COUNT, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onJokesReceived(jokeResponse);
    }

    @Test
    public void should_return_failure_if_repository_failed() {
        // given
        givenRepositoryWillFail();

        // when
        getListOfJokesInteractor.execute(COUNT, callback);
        testScheduler.triggerActions();

        // then
        verify(callback, only()).onJokesFailed();
    }

    private void givenRepositoryWillReturnJokes() {
        when(jokeRepository.getListOfJokes(any())).thenReturn(Single.just(jokeResponse));
    }

    private void givenRepositoryWillFail() {
        when(jokeRepository.getListOfJokes(any())).thenReturn(Single.error(new RuntimeException()));
    }
}
