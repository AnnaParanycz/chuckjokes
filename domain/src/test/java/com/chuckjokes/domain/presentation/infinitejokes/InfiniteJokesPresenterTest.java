package com.chuckjokes.domain.presentation.infinitejokes;

import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.presentation.infinitejokes.model.InfiniteJokesView;
import com.chuckjokes.domain.presentation.jokedetails.navigation.JokeDetailsNavigator;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.GetListOfJokesInteractor;
import com.chuckjokes.domain.usecase.joke.getlistofjokes.interactor.callback.ListOfJokesInteractorCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import io.reactivex.disposables.Disposable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InfiniteJokesPresenterTest {

    private static final String JOKE_ID = "joke_id";
    private static final int JOKES_BATCH_COUNT = 20;
    private static final int DIFFERENT_MENU = 4;
    private static final int UP = 5;

    private final List<Joke> jokes = Arrays.asList(new Joke("Joke 1", "1", ""), new Joke("Joke 2", "2", ""));

    private InfiniteJokesPresenter presenter;

    @Mock
    private GetListOfJokesInteractor getListOfJokesInteractorUseCase;

    @Mock
    private JokeDetailsNavigator jokeDetailsNavigator;

    @Mock
    private NavigationAction navigationAction;

    @Mock
    private InfiniteJokesView view;

    @Before
    public void setup() {
        presenter = new InfiniteJokesPresenter(getListOfJokesInteractorUseCase, jokeDetailsNavigator);

        NavigationAction.setInstance(navigationAction);
        when(navigationAction.getUp()).thenReturn(UP);
        when(jokeDetailsNavigator.id(any())).thenReturn(jokeDetailsNavigator);
    }

    @Test
    public void should_finish_running_jobs_when_up_button_is_clicked() {
        // given
        final Disposable disposable = givenLoadingJokesInInProgress();
        givenViewIsAttached();

        // when
        presenter.onOptionsItemSelected(UP);

        // then
        verify(disposable).dispose();
    }

    private Disposable givenLoadingJokesInInProgress() {
        Disposable disposable = mock(Disposable.class);
        when(getListOfJokesInteractorUseCase.execute(anyInt(), any())).thenReturn(disposable);
        presenter.requestMoreJokes();
        return disposable;
    }

    private void givenViewIsAttached() {
        presenter.attachView(view);
    }

    @Test
    public void should_finish_running_jobs_when_back_button_is_clicked() {
        // given
        final Disposable disposable = givenLoadingJokesInInProgress();
        givenViewIsAttached();

        // when
        presenter.onBackPressed();

        // then
        verify(disposable).dispose();
    }

    @Test
    public void should_not_finish_running_jobs_when_different_menu_button_is_clicked() {
        // given
        final Disposable disposable = givenLoadingJokesInInProgress();
        givenViewIsAttached();

        // when
        presenter.onOptionsItemSelected(DIFFERENT_MENU);

        // then
        verify(disposable, never()).dispose();
    }

    @Test
    public void should_add_jokes_to_view_when_jokes_are_received() {
        // given
        givenJokesUseCaseWillReturnMoreJokes();
        givenViewIsAttached();

        // when
        presenter.requestMoreJokes();

        // then
        verify(view).addJokes(jokes);
        verify(view).showLoaded();
        verify(view, never()).showError();
    }

    private void givenJokesUseCaseWillReturnMoreJokes() {
        when(getListOfJokesInteractorUseCase.execute(anyInt(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, ListOfJokesInteractorCallback.class).onJokesReceived(jokes);
                return mock(Disposable.class);
            }
        });
    }

    @Test
    public void should_show_error_view_when_jokes_are_received() {
        // given
        givenJokesUseCaseWillReturnFailure();
        givenViewIsAttached();

        // when
        presenter.requestMoreJokes();

        // then
        verify(view, never()).addJokes(jokes);
        verify(view, never()).showLoaded();
        verify(view).showError();
    }

    private void givenJokesUseCaseWillReturnFailure() {
        when(getListOfJokesInteractorUseCase.execute(anyInt(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, ListOfJokesInteractorCallback.class).onJokesFailed();
                return mock(Disposable.class);
            }
        });
    }

    @Test
    public void should_show_loaded_when_attaching_if_view_already_has_jokes() {
        // given
        givenViewAlreadyHasJokes();

        // when
        presenter.attachView(view);

        // then
        verify(view).initUi();
        verify(view).showLoaded();
        verify(view, never()).showLoadingScreen();
        verifyZeroInteractions(getListOfJokesInteractorUseCase);
    }

    private void givenViewAlreadyHasJokes() {
        when(view.hasJokes()).thenReturn(true);
    }

    @Test
    public void should_load_jokes_when_attached_if_view_does_not_have_any_jokes() {
        // when
        presenter.attachView(view);

        // then
        verify(view).initUi();
        verify(view, never()).showLoaded();
        verify(view).showLoadingScreen();
        verify(getListOfJokesInteractorUseCase).execute(eq(JOKES_BATCH_COUNT), isA(ListOfJokesInteractorCallback.class));
    }

    @Test
    public void should_load_more_jokes_when_joke_list_reached_bottom() {
        // when
        presenter.onJokeListBottomReached();

        // then
        verify(view, never()).showLoadingScreen();
        verify(getListOfJokesInteractorUseCase).execute(eq(JOKES_BATCH_COUNT), isA(ListOfJokesInteractorCallback.class));
    }

    @Test
    public void should_load_more_jokes_when_retry_clicked() {
        // when
        presenter.onRetryToLoadJokes();

        // then
        verify(view, never()).showLoadingScreen();
        verify(getListOfJokesInteractorUseCase).execute(eq(JOKES_BATCH_COUNT), isA(ListOfJokesInteractorCallback.class));
    }

    @Test
    public void should_navigate_to_joke_details_activity_with_joke_id_when_joke_is_clicked() {
        // given
        givenViewIsAttached();

        // when
        presenter.onJokeListItemClicked(JOKE_ID);

        // then
        InOrder inOrder = Mockito.inOrder(jokeDetailsNavigator);
        inOrder.verify(jokeDetailsNavigator).id(JOKE_ID);
        inOrder.verify(jokeDetailsNavigator).navigate(view);
    }
}
