package com.chuckjokes.domain.presentation.home;

import com.chuckjokes.domain.presentation.home.model.HomeView;
import com.chuckjokes.domain.presentation.infinitejokes.navigation.InfiniteJokesNavigator;
import com.chuckjokes.domain.presentation.personalisedjoke.navigation.PersonalisedJokeNavigator;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.GetRandomJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandomjoke.interactor.callback.RandomJokeInteractorCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.disposables.Disposable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    private static final String RANDOM_JOKE_DIALOG = "random_joke_dialog";
    private static final String RANDOM_JOKE_FAILED_DIALOG = "random_joke_failed_dialog";

    private final Joke joke = new Joke("joke id", "joke display test", "");

    private HomePresenter homePresenter;

    @Mock
    private InfiniteJokesNavigator infiniteJokesNavigator;

    @Mock
    private PersonalisedJokeNavigator personalisedJokeNavigator;

    @Mock
    private GetRandomJokeInteractor getRandomJokeInteractor;

    @Mock
    private Disposable disposable;

    @Mock
    private HomeView homeView;

    @Before
    public void setup() {
        homePresenter = new HomePresenter(
                infiniteJokesNavigator,
                personalisedJokeNavigator,
                getRandomJokeInteractor
        );

        givenGetRandomJokeReturnsDisposableWhenSubscribed();
    }

    @Test
    public void should_init_ui_when_view_is_attached() {
        // when
        homePresenter.attachView(homeView);

        // then
        homeView.initUi();
    }

    @Test
    public void should_navigate_to_infinite_jokes_screen_when_infinite_jokes_button_is_clicked() {
        // given
        givenViewIsAttached();

        // when
        homePresenter.onInfiniteJokesButtonClicked();

        // then
        verify(infiniteJokesNavigator, only()).navigate(homeView);
        verifyZeroInteractions(getRandomJokeInteractor, personalisedJokeNavigator);
    }

    @Test
    public void should_navigate_to_personalised_joke_screen_when_personalised_joke_button_is_clicked() {
        // given
        givenViewIsAttached();

        // when
        homePresenter.onPersonalisedJokeButtonClicked();

        // then
        verify(personalisedJokeNavigator, only()).navigate(homeView);
        verifyZeroInteractions(getRandomJokeInteractor, infiniteJokesNavigator);
    }

    @Test
    public void should_request_random_joke_when_random_joke_button_is_clicked() {
        // given
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(getRandomJokeInteractor, only()).execute(isA(RandomJokeInteractorCallback.class));
        verifyZeroInteractions(infiniteJokesNavigator, personalisedJokeNavigator);
    }

    @Test
    public void should_update_view_ui_to_loading_while_loading_random_joke() {
        // given
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(homeView).disableRandomJokeButton();
        verify(homeView).showLoadingRandomJoke();
    }

    @Test
    public void should_ignore_requesting_random_joke_if_random_joke_is_already_being_requested() {
        // given
        givenViewIsAttached();
        givenRandomJokeIsAlreadyBeingRequested();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(disposable).isDisposed();
        verify(getRandomJokeInteractor, times(1)).execute(any());
    }

    @Test
    public void should_display_joke_dialog_when_random_joke_is_loaded() {
        // given
        givenGetRandomJokeWillBeSuccessful();
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(homeView).displayRandomJokeDialog(RANDOM_JOKE_DIALOG, joke.getDisplayText());
        verify(homeView, never()).displayRandomJokeFailedDialog(any());
    }

    @Test
    public void should_update_view_ui_to_loaded_when_random_joke_is_loaded() {
        // given
        givenGetRandomJokeWillBeSuccessful();
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(homeView).enableRandomJokeButton();
        verify(homeView).hideLoadingRandomJoke();
    }

    @Test
    public void should_display_failed_random_joke_dialog_the_view_when_random_joke_has_failed_to_load() {
        // given
        givenGetRandomJokeWillBeFail();
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(homeView).displayRandomJokeFailedDialog(RANDOM_JOKE_FAILED_DIALOG);
        verify(homeView, never()).displayRandomJokeDialog(any(), any());
    }

    @Test
    public void should_update_view_ui_to_loaded_when_random_joke_is_failed() {
        // given
        givenGetRandomJokeWillBeFail();
        givenViewIsAttached();

        // when
        homePresenter.onRandomJokeButtonClicked();

        // then
        verify(homeView).enableRandomJokeButton();
        verify(homeView).hideLoadingRandomJoke();
    }

    private void givenGetRandomJokeWillBeSuccessful() {
        when(getRandomJokeInteractor.execute(any())).thenAnswer(invocation -> {
            invocation.getArgumentAt(0, RandomJokeInteractorCallback.class).onRandomJokeReceived(joke);
            return mock(Disposable.class);
        });
    }

    private void givenGetRandomJokeWillBeFail() {
        when(getRandomJokeInteractor.execute(any())).thenAnswer(invocation -> {
            invocation.getArgumentAt(0, RandomJokeInteractorCallback.class).onRandomJokeFailed();
            return mock(Disposable.class);
        });
    }

    private void givenGetRandomJokeReturnsDisposableWhenSubscribed() {
        when(getRandomJokeInteractor.execute(any())).thenReturn(disposable);
    }

    private void givenRandomJokeIsAlreadyBeingRequested() {
        when(disposable.isDisposed()).thenReturn(false).thenReturn(true);
        homePresenter.onRandomJokeButtonClicked();
    }

    private void givenViewIsAttached() {
        homePresenter.attachView(homeView);
    }
}
