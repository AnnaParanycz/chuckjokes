package com.chuckjokes.domain.presentation.personalisedjoke;

import com.chuckjokes.domain.api.presentation.navigation.NavigationAction;
import com.chuckjokes.domain.presentation.personalisedjoke.model.PersonalisedJokeView;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.GetRandomPersonalisedJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getrandompersonalisedjoke.interactor.callback.RandomPersonalisedJokeInteractorCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import io.reactivex.disposables.Disposable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("SameParameterValue")
@RunWith(MockitoJUnitRunner.class)
public class PersonalisedJokePresenterTest {

    private static final String PERSONALISED_JOKE_DIALOG = "personalised_joke_dialog";
    private static final String JOKE_FAILED_DIALOG = "joke_failed_dialog";
    private static final String NAME = "joke name testing";
    private static final String INVALID_INPUT = "invalid input";

    private static final int DIFFERENT_MENU = 4;
    private static final int UP = 5;

    private PersonalisedJokePresenter presenter;

    @Mock
    private NavigationAction navigationAction;

    @Mock
    private GetRandomPersonalisedJokeInteractor getRandomPersonalisedJokeInteractor;

    @Mock
    private PersonalisedJokeView view;

    @Before
    public void setup() {
        presenter = new PersonalisedJokePresenter(getRandomPersonalisedJokeInteractor);
        presenter.attachView(view);

        NavigationAction.setInstance(navigationAction);
        when(navigationAction.getUp()).thenReturn(UP);
    }

    @Test
    public void should_init_ui_when_view_is_attached() {
        // then
        verify(view).initUi();
    }

    @Test
    public void should_disable_view_when_find_joke_button_is_clicked() {
        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).showLoading();
        verify(view).disableGetPersonalisedJokeButton();
    }

    @Test
    public void should_request_personalised_joke_when_button_is_clicked() {
        givenViewHasName(NAME);

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).getName();
        verify(getRandomPersonalisedJokeInteractor).execute(eq(NAME), isA(RandomPersonalisedJokeInteractorCallback.class));
    }

    private void givenViewHasName(String name) {
        when(view.getName()).thenReturn(name);
    }

    @Test
    public void should_enable_view_when_failed_to_receive_joke() {
        // given
        givenPersonalisedJokeWIllFail();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).hideLoading();
        verify(view).enableGetPersonalisedJokeButton();
    }

    private void givenPersonalisedJokeWIllFail() {
        when(getRandomPersonalisedJokeInteractor.execute(any(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, RandomPersonalisedJokeInteractorCallback.class).onRandomPersonalisedJokeFailed();
                return mock(Disposable.class);
            }
        });
    }

    @Test
    public void should_show_failure_dialog_when_failed_to_receive_joke() {
        // given
        givenPersonalisedJokeWIllFail();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).displayPersonalisedJokeFailedDialog(JOKE_FAILED_DIALOG);
        verify(view, never()).displayPersonalisedJokeValidationError(any());
    }

    @Test
    public void should_enable_view_when_no_joke_name_provided() {
        // given
        givenPersonalisedJokeWIllFailOnJokeNameValidation();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).hideLoading();
        verify(view).enableGetPersonalisedJokeButton();
    }

    private void givenPersonalisedJokeWIllFailOnJokeNameValidation() {
        when(getRandomPersonalisedJokeInteractor.execute(any(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, RandomPersonalisedJokeInteractorCallback.class).onRandomPersonalisedJokeInvalid(INVALID_INPUT);
                return mock(Disposable.class);
            }
        });
    }

    @Test
    public void should_show_provide_validator_error_when_no_joke_name_provided() {
        // given
        givenPersonalisedJokeWIllFailOnJokeNameValidation();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).displayPersonalisedJokeValidationError(INVALID_INPUT);
        verify(view, never()).displayPersonalisedJokeFailedDialog(JOKE_FAILED_DIALOG);
    }

    @Test
    public void should_enable_view_when_joke_provided() {
        // given
        givenPersonalisedJokeWIllBeSuccessful();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).hideLoading();
        verify(view).enableGetPersonalisedJokeButton();
    }

    private String givenPersonalisedJokeWIllBeSuccessful() {
        final String jokeText = "some text";
        when(getRandomPersonalisedJokeInteractor.execute(any(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, RandomPersonalisedJokeInteractorCallback.class).onRandomPersonalisedJokeJokeReceived(jokeText);
                return mock(Disposable.class);
            }
        });
        return jokeText;
    }

    @Test
    public void should_show_joke_dialog_joke_name_provided() {
        // given
        final String joke = givenPersonalisedJokeWIllBeSuccessful();

        // when
        presenter.onFindJokeButtonClicked();

        // then
        verify(view).displayPersonalisedJokeDialog(PERSONALISED_JOKE_DIALOG, joke);
        verify(view, never()).displayPersonalisedJokeFailedDialog(JOKE_FAILED_DIALOG);
    }

    @Test
    public void should_finish_running_jobs_when_up_button_is_clicked() {
        // given
        final Disposable disposable = givenLoadingJokesInInProgress();

        // when
        presenter.onOptionsItemSelected(UP);

        // then
        verify(disposable).dispose();
    }

    private Disposable givenLoadingJokesInInProgress() {
        Disposable disposable = mock(Disposable.class);
        when(getRandomPersonalisedJokeInteractor.execute(any(), any())).thenReturn(disposable);
        presenter.onFindJokeButtonClicked();
        return disposable;
    }

    @Test
    public void should_not_finish_running_jobs_when_different_menu_button_is_clicked() {
        // given
        final Disposable disposable = givenLoadingJokesInInProgress();

        // when
        presenter.onOptionsItemSelected(DIFFERENT_MENU);

        // then
        verify(disposable, never()).dispose();
    }
}
