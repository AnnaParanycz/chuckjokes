package com.chuckjokes.domain.presentation.jokedetails;

import com.chuckjokes.domain.presentation.jokedetails.model.JokeDetailsView;
import com.chuckjokes.domain.usecase.joke.entity.Joke;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.GetJokeInteractor;
import com.chuckjokes.domain.usecase.joke.getjoke.interactor.callback.GetJokeInteractorCallback;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import io.reactivex.disposables.Disposable;

import static com.chuckjokes.domain.api.testutil.CustomAssertions.argThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JokeDetailsPresenterTest {

    private JokeDetailsPresenter presenter;

    @Mock
    private JokeDetailsView view;

    @Mock
    private GetJokeInteractor getJokeInteractor;

    @Mock
    private Disposable disposable;

    @Before
    public void setup() {
        presenter = new JokeDetailsPresenter(getJokeInteractor);
    }

    @Test
    public void should_initUi_and_load_when_view_is_attached() {
        // given
        String id = givenIdFromView();

        // when
        presenter.attachView(view);

        // then
        verify(view).initUi();
        verify(view).showLoading();
        verify(view).getId();
        verify(getJokeInteractor).execute(argThat(request -> {
            assertThat(request).isNotNull();
            assertThat(request.getId()).isEqualTo(id);
        }), isA(GetJokeInteractorCallback.class));
    }

    @Test
    public void should_load_when_view_reload_is_clicked() {
        // given
        String id = givenIdFromView();

        // when
        presenter.attachView(view);
        presenter.onJokeDetailsErrorClicked();

        // then
        verify(view, times(2)).showLoading();
        verify(view, times(2)).getId();
        verify(getJokeInteractor, times(2)).execute(argThat(request -> {
            assertThat(request).isNotNull();
            assertThat(request.getId()).isEqualTo(id);
        }), isA(GetJokeInteractorCallback.class));
    }

    @Test
    public void should_notify_view_to_display_joke_when_joke_is_received() {
        // given
        Joke joke = givenJokeIsReceivedSuccessfully();

        // when
        presenter.attachView(view);

        // then
        verify(view).showLoadedJoke(joke);
        verify(view, never()).showJokeFailed();
    }

    @Test
    public void should_notify_view_to_display_error_when_joke_fails() {
        // given
        givenJokeFails();

        // when
        presenter.attachView(view);

        // then
        verify(view, never()).showLoadedJoke(any());
        verify(view).showJokeFailed();
    }

    @Test
    public void should_dispose_of_getJokeDisposable_when_detaching() {
        // given
        givenJokeWillReturnDisposable();

        // when
        presenter.attachView(view);
        presenter.detachView();

        // then
        verify(disposable).dispose();
    }

    private void givenJokeFails() {
        when(getJokeInteractor.execute(any(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, GetJokeInteractorCallback.class).onJokeFailed();
                return disposable;
            }
        });
    }

    private void givenJokeWillReturnDisposable() {
        when(getJokeInteractor.execute(any(), any())).thenReturn(disposable);
    }

    private Joke givenJokeIsReceivedSuccessfully() {
        Joke joke = new Joke("some joke", "some id", "Cat1, Cat2");
        when(getJokeInteractor.execute(any(), any())).thenAnswer(new Answer<Disposable>() {
            @Override
            public Disposable answer(InvocationOnMock invocation) throws Throwable {
                invocation.getArgumentAt(1, GetJokeInteractorCallback.class).onJokeReceived(joke);
                return disposable;
            }
        });
        return joke;
    }

    private String givenIdFromView() {
        String id = "joke_id";
        when(view.getId()).thenReturn(id);
        return id;
    }
}
