package com.chuckjokes.domain.api.testutil;

public interface CustomMatcher<T> {

    void assertAll(T argument);
}
