package com.chuckjokes.domain.api.java.stream;

import com.annimon.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ImmutableCollectorsTest {

    private final List<String> originalList = Arrays.asList("One", "Two");

    @Test(expected = UnsupportedOperationException.class)
    public void create_immutable_list() {
        // given
        final Stream<String> listStream = givenListStream();

        // when
        listStream.collect(ImmutableCollectors.toList()).add("Three");
    }

    private Stream<String> givenListStream() {
        return Stream.of(originalList);
    }

    @Test
    public void create_a_correct_list() {
        // given
        final Stream<String> listStream = givenListStream();

        // when
        final List<String> list = listStream.collect(ImmutableCollectors.toList());

        // then
        MatcherAssert.assertThat(list.size(), CoreMatchers.is(2));
        MatcherAssert.assertThat(list.get(0), CoreMatchers.is(CoreMatchers.equalTo(originalList.get(0))));
        MatcherAssert.assertThat(list.get(1), CoreMatchers.is(CoreMatchers.equalTo(originalList.get(1))));
    }
}
