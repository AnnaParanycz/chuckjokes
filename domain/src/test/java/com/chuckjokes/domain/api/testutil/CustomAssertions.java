package com.chuckjokes.domain.api.testutil;

import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;

public class CustomAssertions {

    public static <T> T argThat(final CustomMatcher<T> matcher) {
        return Matchers.argThat(new ArgumentMatcher<T>() {
            @SuppressWarnings("unchecked")
            @Override
            public boolean matches(Object argument) {
                matcher.assertAll((T) argument);
                return true;
            }
        });
    }
}
