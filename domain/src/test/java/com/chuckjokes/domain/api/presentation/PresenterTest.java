package com.chuckjokes.domain.api.presentation;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PresenterTest {

    private static final String EMPTY_VIEW = "empty_view";
    private static final String REAL_VIEW = "real_view";

    private TestPresenter presenter;

    @Before
    public void setup() {
        givenPresenter();
    }

    private void givenPresenter() {
        presenter = new TestPresenter(EMPTY_VIEW);
    }

    @Test
    public void use_empty_view_after_creation() {
        // when
        final String view = presenter.getView();

        // then
        MatcherAssert.assertThat(view, CoreMatchers.is(EMPTY_VIEW));
    }

    @Test
    public void use_real_view_after_real_view_attached() {
        // given
        givenRealViewIsAttached();

        // when
        final String view = presenter.getView();

        // then
        MatcherAssert.assertThat(view, CoreMatchers.is(REAL_VIEW));
    }

    private void givenRealViewIsAttached() {
        presenter.attachView(REAL_VIEW);
    }

    @Test
    public void use_empty_view_after_real_view_detached() {
        // given
        givenRealViewIsDetached();

        // when
        final String view = presenter.getView();

        // then
        MatcherAssert.assertThat(view, CoreMatchers.is(EMPTY_VIEW));
    }

    private void givenRealViewIsDetached() {
        givenRealViewIsAttached();
        presenter.detachView();
    }

    private static final class TestPresenter extends Presenter<String> {

        private TestPresenter(String emptyView) {
            super(emptyView);
        }

        private String getView() {
            return view;
        }
    }
}