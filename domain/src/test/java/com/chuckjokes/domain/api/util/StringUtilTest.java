package com.chuckjokes.domain.api.util;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class StringUtilTest {

    private static final String BLANK_STRING = "     ";
    private static final String SPACE_STRING = " ";
    private static final String EMPTY_STRING = "";
    private static final String NULL_STRING = null;
    private static final String STRING = "S";

    @Test
    @Parameters(method = "isBlankParameters")
    public void return_isBlank_values_correctly(String input, boolean expectedResult) {
        // when
        assertThat(StringUtil.isBlank(input)).isEqualTo(expectedResult);
    }

    @Test
    @Parameters(method = "isBlankParameters")
    public void return_isNotBlank_values_correctly(String input, boolean expectedResult) {
        // when
        assertThat(StringUtil.isNotBlank(input)).isNotEqualTo(expectedResult);
    }

    public Object[][] isBlankParameters() {
        return new Object[][]{
                new Object[]{NULL_STRING, true},
                new Object[]{EMPTY_STRING, true},
                new Object[]{SPACE_STRING, true},
                new Object[]{BLANK_STRING, true},
                new Object[]{STRING, false},
        };
    }
}